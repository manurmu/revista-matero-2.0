<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AsignacionSupervisorCreador extends Model
{
    protected $table = 'usuario_supervisor_asignacion';
}
