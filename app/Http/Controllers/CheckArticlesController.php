<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use App\Review;
use Illuminate\Http\Request;

class CheckArticlesController extends Controller
{
    
    public function viewarticle(){
       
        if (Auth::check()) {
            
            #$getreview = Review::get()->where('idarticle','=',request('idarticle'));
            $getreview = DB::table('comentarios_articulos')
            ->join('users','comentarios_articulos.id_usuario','=','users.id')
            ->select('comentario as review','name','comentarios_articulos.created_at')
            ->where('id_articulo','=',request('idarticle'))
            ->orderby('comentarios_articulos.created_at','desc')
            ->get();
;   
           $articletopic = DB::table('asignacion_tema_articulos')
           ->join('tema_articulos','tema_articulos.id','=','asignacion_tema_articulos.id_tema')
           ->select('tema_articulos.tema')->where('asignacion_tema_articulos.titulo','=',request('articlename'))
           ->get();    
           $articlecategory = DB::table('asignacion_tema_articulos')
           ->join('categoria_articulos','categoria_articulos.id','=','asignacion_tema_articulos.id_categoria')
           ->select('categoria_articulos.categoria','categoria_articulos.descripcion')
           ->where('asignacion_tema_articulos.titulo','=',request('articlename'))
           ->first();        
            $Articlechecked = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->select('users.id_tipo as usertype','users.id as userid','articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('articulos.id','=',request('idarticle'))->get();

            $numerodeaprobados = DB::table('usuario_supervisor_asignacion')
            ->where('id','=',request('idarticle'))
            ->where('aprobado','=',1)
            ->count();
        #return request();
            return view('article',compact('Articlechecked','numerodeaprobados','getreview','articletopic','articlecategory'));
        }
        else{
            return view('welcome');
        }
        
    }
}
