<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Email;

class EmailController extends Controller
{
    public function store(){
        Mail::to("gmanurmu@gmail.com")->send(new Email);
        return "mensaje enviado";
    }
}
