<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Asignacion_tema_articulo;
use App\Peticion;
use App\Articulo;
use Illuminate\Support\Facades\Mail;
use App\Mail\ArticuloNuevo;

use App\Supervisor_especialidad_asigacion;
use Illuminate\Support\Facades\Validator;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
           
        if (Auth::user()->id_tipo == 1) {
            $tipo = 'editor';
           $correcto = 0;
           $supervisor = DB::table('users')
           ->select('id as idsupervisor','name','id_tipo')->where('id_tipo','=','3')->get();
           $creator = DB::table('users')
           ->select('name','id as idcreador')->where('id_tipo','=','2')->get();
            $Articleuploaded = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->select('articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('articulos.id_estado','=','1')->get();;
            $Articlechecked = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->select('articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('articulos.id_estado','=','2')->get();;
            $Articleaprobed = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->select('articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('articulos.id_estado','=','3')->get();;
            $Articlepublished = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->select('articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('articulos.id_estado','=','4')->get();;
            #return $Articlechecked;
            $petitions = DB::table('peticiones')
            ->select('*')
            ->groupBy('name')
            ->get();
            
            
            #return $especialidades_supervisor;
    
            return view('home',compact('tipo','petitions','Articlechecked','Articleaprobed','Articlepublished','Articleuploaded','correcto','supervisor','creator'));
            #return view('home',compact('tipo'));
            #return redirect('/home/'.Auth::user()->name);
        }
        if (Auth::user()->id_tipo == 2) {
            $correcto = 0;
            $tipo = 'lector';
            
            $tipoarticulo = DB::table('categoria_articulos')
            ->select('*','id as categoriaid')
            ->get();
            $temaarticulo = DB::table('tema_articulos')
            ->select('*','id as temaid')
            ->get();

            $tableArticle = DB::table('articulos')->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->orderby('articulos.created_at','desc')
            ->select('*','articulos.created_at as creado','articulos.id as article','articulos.archivo')->where('articulos.id_usuario','=',Auth::user()->id)->get();;
            #return $tableArticle;
            return view('home',compact('tipo','tableArticle','correcto','tipoarticulo','temaarticulo'));
            #return redirect('/home/'.Auth::user()->name);
        }
        if (Auth::user()->id_tipo == 3) {
            $tipo = 'critico';
            $correcto = 0;
            $tipoarticulo = DB::table('categoria_articulos')
            ->select('*','id as categoriaid')
            ->get();
            $temaarticulo = DB::table('tema_articulos')
            ->select('*','id as temaid')
            ->get();
            $articles = DB::table('usuario_supervisor_asignacion')
            ->join('articulos','articulos.id','=','usuario_supervisor_asignacion.id')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->select('articulos.id','usuario_supervisor_asignacion.creador as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('usuario_supervisor_asignacion.supervisor','=',Auth::user()->name)
            ->where('articulos.id_estado','=','1')->get();
            $articlesreviewed = DB::table('usuario_supervisor_asignacion')
            ->join('articulos','articulos.id','=','usuario_supervisor_asignacion.id')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->select('articulos.id','usuario_supervisor_asignacion.creador as username','usuario_supervisor_asignacion.aprobado','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('usuario_supervisor_asignacion.supervisor','=',Auth::user()->name)
            ->where('articulos.id_estado','=','2')->get();
            
            return view('home',compact('tipo','articles','articlesreviewed','correcto','tipoarticulo','temaarticulo'));
        }
        else{
            $tipo ='default';
            $specialties = DB::table('supervisor_especialidades')
            ->select('*')->get();
            return redirect('/');
            #return view('home',compact('specialties','tipo'));
        }
    }
    
    public function savearticle(Request $request){
       # return request();
        #return request();
        
        
        $usuario = Auth::user()->name;
        $request->validate([
            'articletitle' => 'unique:articulos,titulo|required',
            'content' => 'required',
            'tipoarticulo' => 'required',
            
            'article' => 'mimetypes:application/pdf,application/x-latex,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        ],[
            'articletitle.unique' => 'Este titulo ya existe',
            'article.mimetypes' => 'El archivo debe ser del tipo PDF,LATEX,DOC O DOCX',
            'articletitle.required' => 'Este campo es necesario'
        ]);
        #request()->validate([
        #    'article' => 'mimes:pdf'
        #]);
        $tableArticle = new Articulo;
        $tableArticle->titulo= request('articletitle');
        $tableArticle->resumen = request('content');
        $tableArticle->id_usuario = Auth::user()->id;
        $tableArticle->id_estado = '1';
        
        $tableArticle->asignado = '0';
        $tableArticle->autores = request('author');
        #$tableArticle->articlefile = request()->file('article')->storeAs('public',request('articletitle').'.pdf');
        $tableArticle->archivo = request()->file('article')->storeAs('public',Auth::user()->id.request()->file('article')->getClientOriginalName());
        $tableArticle->save();

        

        $articleassignment = new Asignacion_tema_articulo;
        $articleassignment->titulo = request('articletitle');
        $articleassignment->id_tema = request('tema1');
        $articleassignment->id_categoria = request('tipoarticulo');
        $articleassignment->save();

        if (request('tema2') != null) {
            $articleassignment2 = new Asignacion_tema_articulo;
            $articleassignment2->titulo = request('articletitle');
            $articleassignment2->id_tema = request('tema2');
            $articleassignment2->id_categoria = request('tipoarticulo');
            $articleassignment2->save();
        }

        Mail::to('jodaurmu@gmail.com')->send(new ArticuloNuevo);

        return redirect('/home')->with(['success'=>1]);
        

        
    }
    public function updatearticle(){

        DB::table('articulos')
        ->where('id',request('idarticle'))
        ->update(['archivo'=> request()->file('article')->storeAs('public',request()->file('article')->getClientOriginalName())]);
        return redirect('/home');
    }
    public function downloadarticle(){
        $numerodescargas = DB::table('articulos')
        ->select('descargas')
        ->where('archivo',request('article'))
        ->first();
        #return $numerodescargas->descargas + 1;
        




        DB::table('articulos')
        ->where('archivo',request('article'))
        ->update(['descargas'=> $numerodescargas->descargas + 1]);

        $fileurl =Storage::url(request('article'));
        return Storage::download(request('article'));
    }
    public function viewcomite(){
        return view('comite');
    }
    
    public function viewpetitions(){
        $specialties = DB::table('peticiones')
        ->join('supervisor_especialidades','supervisor_especialidades.id','=','peticiones.especialidad')
        ->select('supervisor_especialidades.especialidad')->where('id_usuario','=',request('iduser'))
        ->get();
        $nombreapellido = DB::table('peticiones')
        ->select('nombres','apellidos')->where('id_usuario','=',request('iduser'))
        ->first();

        $username = request('username');  
        $idusuario = request('iduser');  
        return view('petitions',compact('username','specialties','nombreapellido','idusuario'));
    }
    public function aprobepetition(){
        DB::table('users')
        ->where('name',request('username'))
        ->update(['id_tipo' => '3']);
        $especialidades = DB::table('peticiones')
        ->select('especialidad')
        ->where('name','=',request('username'))
        ->get();

        foreach ($especialidades as $item) {

            $tabla_asignaciones = new Supervisor_especialidad_asigacion;
            $tabla_asignaciones->id_supervisor = request('idusuario');
            $tabla_asignaciones->id_especialidad = $item->especialidad;
            $tabla_asignaciones->save();
        }

        DB::table('peticiones')
        ->where('name','=',request('username'))
        ->delete();


        
        return redirect('/home')->with(['aprobed' => 1]);
    }
    public function denypetition(){
        DB::table('peticiones')
        ->where('name', '=', request('username'))
        ->delete();
        return redirect('/home')->with(['denied'=>1]);
    }
    
    public function ajax(Request $request){
        $supervisortopics = DB::table('supervisor_especialidad_asignaciones')
        ->select('id_especialidad')->where('id_supervisor','=',$request->iduser)->get();
       
          
        return response()->json(['request'=>$supervisortopics]);
    }
    public function consultartipocategoria(Request $request){
        $categoriaarticulo = DB::table('categoria_articulos')
        ->select('descripcion')->where('id','=',$request->categoria)->get();
       
          
        return response()->json(['request'=>$categoriaarticulo]);
    }
    
   
    
}
