<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LlenarDatos extends Controller
{
    public function llenar(Request $request){
        $request->validate([
            'nombre' => 'required|alpha',
            'apellido' => 'required|alpha',
            'edad' => 'required|digits:2|digits_between:18,100',
            
            
        ],[
            'nombre.alpha' => 'Debe contener solamente letras',
            'apellido.alpha' => 'Debe contener solamente letras',
            'edad.digits' => 'Debe contener solamente numeros',
            'edad.digits_between' => 'Tienes que tener como minimo 18 años',
        ]);



        return request();
    }
}
