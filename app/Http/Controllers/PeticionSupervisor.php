<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peticion;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\NuevaPeticion;

class PeticionSupervisor extends Controller
{
    public function peticion(Request $request){

        $request->validate([
            'nombres' => 'required',
            'apellidos' => 'required',
            'especialidad' => 'required|array|max:3',
            'especialidad.*' => 'required|string|distinct',
        ],[
            'especialidad.max' => 'Escoge maximo 3 especialidades',
            'especialidad.required' => 'Escoge maximo 3 especialidades',
        ]);
        $especialidades = request('especialidad');
        foreach ($especialidades as $item) {

            $tabla_peticion = new Peticion;
            $tabla_peticion->id_usuario = Auth::user()->id;
            $tabla_peticion->name = Auth::user()->name;
            $tabla_peticion->especialidad = $item;
            $tabla_peticion->nombres = request('nombres');
            $tabla_peticion->apellidos = request('apellidos');
            $tabla_peticion->save();
        }

          DB::table('users')
        ->where('id',Auth::user()->id)
        ->update(['id_tipo'=> 4]);  

        Mail::to('jodaurmu@gmail.com')
        ->send(new NuevaPeticion); 
        


        




        return redirect('/')->with(['success'=>1]);

    }
}
