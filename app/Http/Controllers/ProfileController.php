<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use App\AsignacionSupervisorCreador;
use App\CompletarRegistro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ArticuloAsignado;
use App\Mail\RegistroUsuario;

class ProfileController extends Controller
{
    public function viewprofile(){
        #SELECT articles.name,users.name,assignments.supervisor from articles INNER join users on articles.iduser = users.id INNER JOIN 
        #assignments on assignments.idarticlee = articles.id where articles.assigned = 1
        $username = request('username');
        $usertype = request('usertype');
        $iduser = request('iduser');
        
        $supervisortopics = DB::table('supervisor_especialidad_asignaciones')
        ->select('id_especialidad')->where('id_supervisor','=',$iduser)->get();
        $assigned = DB::table('articulos')
        ->join('users','articulos.id_usuario','=','users.id')
        ->join('usuario_supervisor_asignacion','usuario_supervisor_asignacion.id','=','articulos.id')
        ->select('articulos.titulo','supervisor')->where('users.name','=',$username)->where('asignado','=','1')
        ->get();
        $unassigned = DB::table('articulos')
        ->join('users','articulos.id_usuario','=','users.id')
        ->select('articulos.titulo','articulos.id')->where('users.name','=',$username)->where('asignado','=','0')
        ->get();
        $supervisors = DB::table('users')
        ->select('name')->where('id_tipo','=','3')
        ->get();
        $articlesreviewed = DB::table('usuario_supervisor_asignacion')
            ->join('articulos','articulos.id','=','usuario_supervisor_asignacion.id')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->select('articulos.id','usuario_supervisor_asignacion.creador as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('usuario_supervisor_asignacion.supervisor','=',$username)
            ->get();
                #return $assigned;
        return view('templates.user',compact('username','usertype','assigned','unassigned','supervisors','articlesreviewed','supervisortopics'));
    }
    public function listarcargo(){
        if (Auth::check()) {
            if (Auth::user()->id_tipo == '4') {
                $cargos = DB::table('cargo_usuario')
                ->select('*')
                ->get();
                return view('completarregistro',compact('cargos'));
            } else {
                return redirect('/');
            }
            
            
        } else {
            return redirect('/');
        }
        
       
    }
    public function completar_registro_existente(){
        return view('completarregistroexistente');
    }
    public function completar_registro_existente_final(Request $request){
        $request->validate([
            'peticion_cargo' => 'required',
        ],[
            'peticion_cargo.required' => 'Campo obligatorio',
        ]);

        DB::table('users')
        ->where('name',Auth::user()->name)
        ->update(['id_tipo' => '5']);

        Mail::to('fmanurmu@gmail.com')->send(new RegistroUsuario);

        return redirect('/')->with(['registrado_solicitud' => 1]);
    }
    public function completar_registro(Request $request){


        $request->validate([
            'nombre' => 'required|string',
            'apellido' => 'required|string',
            'afiliacion' => 'required',
            'cargo' => 'required',
        ],[
            'nombre.string' => 'Ingrese solo caracteres',
            'apellido.string' => 'Ingrese solo caracteres',
            
        ]);

        $nombre = request('nombre');
        $apellido = request('apellido');
        $afiliacion = request('afiliacion');
        $cargo = request('cargo');
        $peticion = request('peticion_cargo');
        $check = request('check_lector');

       
        

        
        
        $usuarionuevo = new CompletarRegistro;
        $usuarionuevo->id = Auth::user()->id;
        $usuarionuevo->nombre=$nombre;
        $usuarionuevo->apellido=$apellido;
        $usuarionuevo->afiliacion=$afiliacion;
        $usuarionuevo->cargo=$cargo;
        $usuarionuevo->peticion=$peticion;
        $usuarionuevo->save();

        if ($check == 'on') {

            DB::table('users')
        ->where('name',Auth::user()->name)
        ->update(['id_tipo' => '5']);

        Mail::to('fmanurmu@gmail.com')->send(new RegistroUsuario);

        return redirect('/')->with(['registrado_solicitud' => 1]);
        }
        else{
            DB::table('users')
        ->where('name',Auth::user()->name)
        ->update(['id_tipo' => '5']);

        return redirect('/')->with(['registrado' => 1]);

        }
        

        

        

        
    }
    public function assignsupervisor(){
        $correcto = 0;
        
        $tableAssignment = new AsignacionSupervisorCreador;
        $tableAssignment->supervisor = request('supervisor1');
        $tableAssignment->creador = request('username');
        $tableAssignment->id = request('idarticle'); 
        $tableAssignment->aprobado = 0;
        $tableAssignment->supervisor_id = 1;
        $tableAssignment->save();

        if (request('supervisor2') != null) {
            $tableAssignment = new AsignacionSupervisorCreador;
        $tableAssignment->supervisor = request('supervisor2');
        $tableAssignment->creador = request('username');
        $tableAssignment->id = request('idarticle'); 
        $tableAssignment->aprobado = 0;
        $tableAssignment->supervisor_id = 2;
        $tableAssignment->save();
        }
        
        if (request('supervisor3') != null) {
            $tableAssignment = new AsignacionSupervisorCreador;
        $tableAssignment->supervisor = request('supervisor3');
        $tableAssignment->creador = request('username');
        $tableAssignment->id = request('idarticle'); 
        $tableAssignment->aprobado = 0;
        $tableAssignment->supervisor_id = 3;
        $tableAssignment->save();
        }
        
        $supervisor_destinatario1 =  DB::table('users')
        ->select('email')
        ->where('name','=',request('supervisor1'))
        ->first();
        $supervisor_destinatario2 =  DB::table('users')
        ->select('email')
        ->where('name','=',request('supervisor2'))
        ->first();
        $supervisor_destinatario3 =  DB::table('users')
        ->select('email')
        ->where('name','=',request('supervisor3'))
        ->first();    


        DB::table('articulos')
        ->where('id',request('idarticle'))
        ->update(['asignado'=> 1]);

        Mail::to($supervisor_destinatario1->email)
        ->cc($supervisor_destinatario2->email)
        ->bcc($supervisor_destinatario3->email)
        ->send(new ArticuloAsignado);


        return redirect('/home')->with(['status'=> 1]);
    }
    public function verperfilbasico(Request $request){
        $numerodearticulos = DB::table('usuario_supervisor_asignacion')
        ->select(DB::raw('COUNT(supervisor) as numeroarticulos'))
        ->where('supervisor','=',$request->usuario)
        ->get();
        $usuario= DB::table('users')
        ->select('name as supervisor')
        ->where('name','=',$request->usuario)
        ->get();
        $especialidades_supervisor = DB::table('supervisor_especialidad_asignaciones')
            ->join('supervisor_especialidades','supervisor_especialidad_asignaciones.id_especialidad','=','supervisor_especialidades.id')
            ->select('supervisor_especialidades.especialidad')
            ->where('supervisor_especialidad_asignaciones.id_supervisor','=',$request->id_usuario)
            ->get();
        
        
        return response()->json(['request'=>$usuario,'numero'=>$numerodearticulos,'especialidades'=>$especialidades_supervisor]);
        
    }

}
