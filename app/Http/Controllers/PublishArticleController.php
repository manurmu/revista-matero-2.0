<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;

class PublishArticleController extends Controller
{
    public function changearticlestatus(){

        DB::table('articulos')
        ->where('id',request('articleid'))
        ->update(['id_estado'=> 4]);
        
        return redirect('/home');
    }
    public function viewpublishedarticles(){
        $Articlepublished = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->join('asignacion_tema_articulos','asignacion_tema_articulos.titulo','=','articulos.titulo')
            ->join('categoria_articulos','categoria_articulos.id','=','asignacion_tema_articulos.id_categoria')
            ->groupBy('articulos.titulo')
            ->orderby('created_at','desc')
            ->select('articulos.descargas','categoria_articulos.categoria','articulos.autores','articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->where('articulos.id_estado','=','4')->paginate(4);;
            
            $tipoarticulo = DB::table('categoria_articulos')
            ->select('*','id as categoriaid')
            ->get();
            $temaarticulo = DB::table('tema_articulos')
            ->select('*','id as temaid')
            ->get();
        
            return view('published',compact('Articlepublished','tipoarticulo','temaarticulo'));
    }
    public function vertodosarticulos(Request $request){
        $articulos= DB::table('articulos')
        ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
        ->join('users','articulos.id_usuario','=','users.id')
        ->orderby('created_at','desc')
        ->select('articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')->get();
        
        return response()->json(['request'=>$articulos]);
        
    }
    public function cargardescripcion(Request $request){
        $descripcion= DB::table('tema_articulos')
        ->select('descripcion','tema','id')
        ->where('id','=',$request->idtema)
        ->get();

        return response()->json(['request'=>$descripcion]);

    }
   
    public function ajaxlistararticulos(Request $request){
        if ($request->texto != "") {
            $Articlepublished = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->join('asignacion_tema_articulos','asignacion_tema_articulos.titulo','=','articulos.titulo')
            ->join('categoria_articulos','categoria_articulos.id','=','asignacion_tema_articulos.id_categoria')
            ->select('articulos.descargas','articulos.autores','categoria_articulos.categoria','articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')
            ->where('articulos.id_estado','=','4')->where('articulos.resumen','like','%'.$request->texto.'%')
            ->groupBy('articulos.titulo')
            ->orderby('created_at','desc')
            ->get();
        }
        elseif ($request->tipo == 0 && $request->tema == 0) {
            $Articlepublished = DB::table('articulos')
        ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
        ->join('users','articulos.id_usuario','=','users.id')
        ->join('asignacion_tema_articulos','asignacion_tema_articulos.titulo','=','articulos.titulo')
        ->join('categoria_articulos','categoria_articulos.id','=','asignacion_tema_articulos.id_categoria')
        ->select('articulos.descargas','articulos.autores','categoria_articulos.categoria','articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')
        ->where('articulos.id_estado','=','4')
        ->groupBy('articulos.titulo')
        ->orderby('created_at','desc')
        ->get();
        }
        elseif ($request->tipo == 0 && $request->tema != 0) {
            $Articlepublished = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->join('asignacion_tema_articulos','asignacion_tema_articulos.titulo','=','articulos.titulo')
            ->join('categoria_articulos','categoria_articulos.id','=','asignacion_tema_articulos.id_categoria')
            ->select('articulos.descargas','articulos.autores','categoria_articulos.categoria','articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')
            ->where('articulos.id_estado','=','4')->where('asignacion_tema_articulos.id_tema','=',$request->tema)
            ->groupBy('articulos.titulo')
            ->orderby('created_at','desc')
            ->get();
        }
        elseif ($request->tipo != 0 && $request->tema == 0) {
            $Articlepublished = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->join('asignacion_tema_articulos','asignacion_tema_articulos.titulo','=','articulos.titulo')
            ->join('categoria_articulos','categoria_articulos.id','=','asignacion_tema_articulos.id_categoria')
            ->select('articulos.descargas','articulos.autores','categoria_articulos.categoria','articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')
            ->where('articulos.id_estado','=','4')->where('asignacion_tema_articulos.id_categoria','=',$request->tipo)
            ->groupBy('articulos.titulo')
            ->orderby('created_at','desc')
            ->get();
        }
        
        else{
            $Articlepublished = DB::table('articulos')
            ->join('estado_articulos','articulos.id_estado','=','estado_articulos.id')
            ->join('users','articulos.id_usuario','=','users.id')
            ->join('asignacion_tema_articulos','asignacion_tema_articulos.titulo','=','articulos.titulo')
            ->join('categoria_articulos','categoria_articulos.id','=','asignacion_tema_articulos.id_categoria')
            ->select('articulos.descargas','articulos.autores','categoria_articulos.categoria','articulos.archivo','articulos.id','users.name as username','articulos.created_at','articulos.titulo','articulos.resumen','estado_articulos.estado','articulos.id_estado')
            ->where('articulos.id_estado','=','4')->where('asignacion_tema_articulos.id_categoria','=',$request->tipo)->where('asignacion_tema_articulos.id_tema','=',$request->tema)
            ->get();
        }
       
        return response()->json(['request'=>$Articlepublished]);
    }
    
}
