<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\MensajeEnviado;
use App\Mail\ArticuloAprobado;
use App\Comentario;
use DB;
use Auth;

class ReviewController extends Controller
{
    public function viewreview(){
       
    }
    
    public function createreview(){
        
        
        $review = new Comentario;
        $review->id_articulo = request('idarticle');
        $review->id_usuario = request('iduser');
        $review->comentario = request('content');
        $review->save();
        
        
        DB::table('articulos')
        ->where('id',request('idarticle'))
        ->update(['id_estado'=> 2]);
        
        $destinatario = DB::table('articulos')
        ->join('users','articulos.id_usuario','=','users.id')
        ->select('users.email')
        ->where('articulos.id','=',request('idarticle'))
        ->first();

        #return $destinatario->email;
        

        Mail::to($destinatario->email)->send(new MensajeEnviado);

        return redirect('/home')->with(['status'=> 1]);
         
    }
    
    public function aprobereview(){
        
        DB::table('usuario_supervisor_asignacion')
        ->where('id',request('idarticle'))
        ->where('supervisor',Auth::user()->name)
        ->update(['aprobado'=> 1]);

        
        
      
        $numerodeaprobados = DB::table('usuario_supervisor_asignacion')
            ->where('id','=',request('idarticle'))
            ->where('aprobado','=',1)
            ->count();
            
        if ($numerodeaprobados == 3) {

        DB::table('articulos')
        ->where('id',request('idarticle'))
        ->update(['id_estado'=> 3]); 

        Mail::to('jodaurmu@gmail.com')->send(new ArticuloAprobado);
        }
        
        


        return redirect('/home');
        
    }
    
    
}
