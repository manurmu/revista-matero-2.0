<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VistaController extends Controller
{
    public function GetVista($vista){
        switch ($vista) {
            case 'published':
                return view('published');
            case 'acerca':
                return view('acercade');
            case 'comite':
                return view('comite');
            case 'politica':
                return view('politica');
            case 'perfil':
                return view('perfil');
            case 'llenar':
                return view('templates.llenardatos');
            default:
                 return redirect('/');
          
        }}
}
