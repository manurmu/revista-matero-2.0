<?php

namespace App\Mail;
use Auth;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use DB;

class RegistroUsuario extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     protected $nombre;
     protected $apellido;
     protected $afiliacion;
     protected $cargo;
     protected $peticion;
    public function __construct()
    {
        $this->nombre = DB::table('perfil_usuario')->select('nombre')->where('id','=',Auth::user()->id)->get();
        $this->apellido = DB::table('perfil_usuario')->select('apellido')->where('id','=',Auth::user()->id)->get();
        $this->afiliacion = DB::table('perfil_usuario')->select('afiliacion')->where('id','=',Auth::user()->id)->get();
        $this->cargo = DB::table('perfil_usuario')->select('cargo')->where('id','=',Auth::user()->id)->get();
        $this->peticion = DB::table('perfil_usuario')->select('peticion')->where('id','=',Auth::user()->id)->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.registro_usuario')
        ->with([
            'nombre' => $this->nombre,
            'apellido' => $this->apellido,
            'afiliacion' => $this->afiliacion,
            'cargo' => $this->cargo,
            'peticion' => $this->peticion,
        ]);
    }
}
