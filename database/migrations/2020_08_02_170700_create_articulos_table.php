<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo')->unique();
            $table->longText('resumen');
            $table->integer('id_usuario')->unsigned();
            $table->integer('id_estado')->unsigned();
            $table->integer('descargas')->unsigned();
            $table->string('archivo');
            $table->longText('autores');
            $table->integer('asignado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
