<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Relaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        
        Schema::table('articulos', function (Blueprint $table) {
            $table->foreign('id_usuario')->references('id')->on('users');
            $table->foreign('id_estado')->references('id')->on('estado_articulos');
        });
        Schema::table('asignacion_tema_articulos', function (Blueprint $table) {
            $table->foreign('titulo')->references('titulo')->on('articulos');
            $table->foreign('id_tema')->references('id')->on('tema_articulos');
            $table->foreign('id_categoria')->references('id')->on('categoria_articulos');
        });
        Schema::table('comentarios_articulos', function (Blueprint $table) {
            $table->foreign('id_articulo')->references('id')->on('articulos');
            $table->foreign('id_usuario')->references('id')->on('users');
            
        });
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('id_tipo')->references('id')->on('tipo_usuarios');
            $table->foreign('id_estado')->references('id')->on('estado_usuarios');
        });
        Schema::table('perfil_usuarios', function (Blueprint $table) {
            $table->foreign('id')->references('id')->on('users');
        });
        Schema::table('supervisor_especialidad_asignaciones', function (Blueprint $table) {
            $table->foreign('id_supervisor')->references('id')->on('users');
            $table->foreign('id_especialidad')->references('id')->on('supervisor_especialidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
