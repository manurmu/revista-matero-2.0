-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-10-2020 a las 16:28:28
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `matero2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resumen` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `id_estado` int(10) UNSIGNED NOT NULL,
  `descargas` int(10) UNSIGNED NOT NULL,
  `archivo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `autores` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `asignado` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `titulo`, `resumen`, `id_usuario`, `id_estado`, `descargas`, `archivo`, `autores`, `asignado`, `created_at`, `updated_at`) VALUES
(15, 'Methanogens and Methanotrophs Show Nutrient-Dependent', 'Tropical peatlands are globally important carbon reservoirs that play a crucial role in\r\nfluxes of atmospheric greenhouse gases. Amazon peatlands are expected to be large\r\nsource of atmospheric methane (CH 4 ) emissions, however little is understood about\r\nthe rates of CH 4 flux or the microorganisms that mediate it in these environments.\r\nHere we studied a mineral nutrient gradient across peatlands in the Pastaza-\r\nMarañón Basin, the largest tropical peatland in South America, to describe CH 4 fluxes\r\nand environmental factors that regulate species assemblages of methanogenic and\r\nmethanotrophic microorganisms. Peatlands were grouped as minerotrophic, mixed and\r\nombrotrophic categories by their general water source leading to different mineral\r\nnutrient content (rich, mixed and poor) quantified by trace elements abundance.\r\nMicrobial communities clustered dependent on nutrient content (ANOSIM p < 0.001).\r\nHigher CH 4 flux was associated with minerotrophic communities compared to\r\nthe other categories. The most dominant methanogens and methanotrophs were\r\nrepresented by Methanobacteriaceae, and Methylocystaceae, respectively. Weighted\r\nnetwork analysis demonstrated tight clustering of most methanogen families with\r\nminerotrophic-associated microbial families. Populations of Methylocystaceae were\r\npresent across all peatlands. Null model testing for species assemblage patterns and\r\nspecies rank distributions confirmed non-random aggregations of Methylococcacae\r\nmethanotroph and methanogen families (p < 0.05). We conclude that in studied\r\namazon peatlands increasing mineral nutrient content provides favorable habitats for\r\nMethanobacteriaceae, while Methylocystaceae populations seem to broadly distribute\r\nindependent of nutrient content.', 1, 4, 2, 'public/1Tablas_de_Tukey_y_Duncan.pdf', 'Marco Andre Urquiza Muñoz', 1, '2020-10-24 14:16:10', '2020-10-24 14:16:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion_tema_articulos`
--

CREATE TABLE `asignacion_tema_articulos` (
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_tema` int(10) UNSIGNED NOT NULL,
  `id_categoria` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `asignacion_tema_articulos`
--

INSERT INTO `asignacion_tema_articulos` (`titulo`, `id_tema`, `id_categoria`, `created_at`, `updated_at`) VALUES
('Methanogens and Methanotrophs Show Nutrient-Dependent', 1, 1, '2020-10-24 14:16:10', '2020-10-24 14:16:10'),
('Methanogens and Methanotrophs Show Nutrient-Dependent', 2, 1, '2020-10-24 14:16:10', '2020-10-24 14:16:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_articulos`
--

CREATE TABLE `categoria_articulos` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categoria_articulos`
--

INSERT INTO `categoria_articulos` (`id`, `categoria`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'investigacion original', 'Los artículos de investigación originales informan sobre estudios primarios e inéditos. La investigación original también puede abarcar estudios confirmatorios y resultados no confirmados que permitan eliminar, reformular y / o informar sobre la no reproducibilidad de resultados publicados anteriormente. Los artículos de investigación originales son revisados ​​por pares, tienen un número máximo de palabras de 12,000 y no pueden contener más de 15 figuras / tablas. Los autores deben pagar una tarifa (artículo tipo A) para publicar un artículo de investigación original. Los artículos originales de investigación deben tener el siguiente formato: 1) Resumen, 2) Introducción, 3) Materiales y métodos, 4) Resultados, 5) Discusión.', NULL, NULL),
(2, 'metodos', 'Los artículos sobre métodos presentan un método, protocolo o técnica nuevo o establecido que es de gran interés en el campo. Los artículos de métodos son revisados ​​por pares, tienen un número máximo de palabras de 12,000 y no pueden contener más de 15 figuras / tablas. Los autores deben pagar una tarifa (artículo tipo A) para publicar un artículo de Methods. Los artículos del método deben tener el siguiente formato: 1) Resumen, 2) Introducción (que describe el protocolo y sus posibles aplicaciones), 3) Materiales y equipos (incluida una lista de reactivos / materiales y / o equipos necesarios; formulación de cualquier solución cuando corresponda ), 4) Métodos (incluidos los objetivos y la validación del método; procedimientos paso a paso; calendario de cada paso o serie de pasos relacionados; puntos de pausa; ejemplo (s) de aplicación y eficacia; detalles de precisión / exactitud y límites de detección o cuantificación, cuando corresponda), 5) Resultados (anticipados) (que describen e ilustran con cifras, cuando sea posible, el resultado esperado del protocolo; ventajas, limitaciones, posibles trampas y artefactos y cualquier medida de solución de problemas para contrarrestarlos), 6 ) Discusión. Cualquier método analítico aplicado a los datos generados por el protocolo debe ser referenciado o descrito. Los resultados deben poder reproducirse.', NULL, NULL),
(3, 'revision', 'Los artículos de revisión cubren temas que han experimentado un desarrollo o progreso significativo en los últimos años, con una profundidad integral y una perspectiva equilibrada. Las reseñas deben presentar una descripción general completa del estado del arte (y no deben simplemente resumir la literatura), así como discutir lo siguiente: 1) Diferentes escuelas de pensamiento o controversias, 2) Conceptos, cuestiones y problemas fundamentales, 3) Lagunas de investigación actuales, 4) Desarrollos potenciales en el campo. Los artículos de revisión son revisados ​​por pares, tienen un número máximo de palabras de 12,000 y no pueden contener más de 15 figuras / tablas. Los autores deben pagar una tarifa (artículo tipo A) para publicar un artículo de revisión. Los artículos de revisión deben tener el siguiente formato: 1) Resumen, 2) Introducción, 3) Subsecciones relevantes para el tema, 4) Discusión. Los artículos de revisión no deben incluir material inédito (datos inéditos / originales, manuscritos enviados o comunicaciones personales) y pueden ser rechazados en revisión o reclasificados, con una demora significativa, si se encuentra que incluyen dicho contenido.', NULL, NULL),
(4, '\r\nRevisiones de políticas y prácticas', '\r\nLas revisiones de políticas y prácticas brindan una cobertura integral y una visión general equilibrada de temas actuales y relevantes relacionados con políticas, regulaciones y pautas que pueden provenir de la academia, sociedades relevantes, organismos reguladores, industrias y otros. A diferencia de los Policy Briefs, este tipo de artículo ofrece a los autores más espacio para desarrollar políticas y / o directrices. Las revisiones de políticas y prácticas son revisadas por pares, tienen un número máximo de palabras de 12,000 y no pueden contener más de 15 figuras / tablas. Los autores deben pagar una tarifa (artículo tipo A) para publicar una revisión de políticas y prácticas. Las revisiones de políticas y prácticas deben tener el siguiente formato: 1) Resumen, 2) Introducción, 3) Secciones sobre evaluación de opciones e implicaciones de políticas / directrices, 4) Recomendaciones procesables, 5) Discusión.', NULL, NULL),
(5, 'mini revision', 'Los artículos de Mini Review cubren aspectos específicos de un área de investigación actual y sus desarrollos recientes. Ofrecen un resumen conciso y claro del tema, lo que permite a los lectores estar al día sobre nuevos desarrollos y / o conceptos emergentes, así como discutir lo siguiente: 1) Diferentes escuelas de pensamiento o controversias, 2) Lagunas de investigación actuales. , 3) Desarrollos potenciales futuros en el campo. Los artículos de Mini Reviews son revisados ​​por pares, tienen un número máximo de palabras de 3,000 y no pueden contener más de 2 figuras / tablas. Los autores deben pagar una tarifa (artículo de tipo B) para publicar un artículo de Mini Review. Los artículos de Mini Review deben tener el siguiente formato: 1) Resumen, 2) Introducción, 3) Subsecciones relevantes para el tema, 4) Discusión. Los artículos de Mini Review no deben incluir material inédito (datos inéditos / originales, manuscritos enviados o comunicaciones personales) y pueden ser rechazados o reclasificados, con una demora significativa, si se encuentra que incluyen dicho contenido.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios_articulos`
--

CREATE TABLE `comentarios_articulos` (
  `id_articulo` int(10) UNSIGNED NOT NULL,
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `comentario` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `comentarios_articulos`
--

INSERT INTO `comentarios_articulos` (`id_articulo`, `id_usuario`, `comentario`, `created_at`, `updated_at`) VALUES
(15, 2, 'muy bien,  pero tienes que arreglar la seccion 2', '2020-10-24 14:22:43', '2020-10-24 14:22:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_articulos`
--

CREATE TABLE `estado_articulos` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estado_articulos`
--

INSERT INTO `estado_articulos` (`id`, `estado`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Subido', NULL, NULL, NULL),
(2, 'Revision', NULL, NULL, NULL),
(3, 'Aprobado', NULL, NULL, NULL),
(4, 'Publicado', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_usuarios`
--

CREATE TABLE `estado_usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_02_170700_create_articulos_table', 1),
(5, '2020_08_02_171630_create_asignacion_tema_articulos_table', 1),
(6, '2020_08_02_172626_create_estado_usuarios_table', 1),
(7, '2020_08_02_173052_create_estado_articulos_table', 1),
(8, '2020_08_02_173158_create_categoria_articulos_table', 1),
(9, '2020_08_02_174041_create_tipo_usuarios_table', 1),
(10, '2020_08_02_174242_create_comentarios_articulos_table', 1),
(11, '2020_08_02_175332_create_supervisor_especialidades_table', 1),
(12, '2020_08_02_175553_create_tema_articulos_table', 1),
(13, '2020_08_02_175716_create_supervisor_especialidad_asignaciones_table', 1),
(14, '2020_08_02_180145_create_perfil_usuarios_table', 1),
(15, '2020_08_02_180408_create_usuario_supervisor_asignacion_table', 1),
(16, '2020_08_02_182104_relaciones', 1),
(17, '2020_08_07_152518_create_peticiones_table', 1),
(18, '2020_08_08_012103_inertardatos', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_usuarios`
--

CREATE TABLE `perfil_usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peticiones`
--

CREATE TABLE `peticiones` (
  `id_usuario` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `petitiontype` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supervisor_especialidades`
--

CREATE TABLE `supervisor_especialidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `especialidad` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `supervisor_especialidades`
--

INSERT INTO `supervisor_especialidades` (`id`, `especialidad`, `created_at`, `updated_at`) VALUES
(1, 'suelos', NULL, NULL),
(2, 'botanica', NULL, NULL),
(3, 'inventarios forestales', NULL, NULL),
(4, 'analisis espacial sig', NULL, NULL),
(5, 'genetica', NULL, NULL),
(6, 'estadistica', NULL, NULL),
(7, 'fauna', NULL, NULL),
(8, 'fisiologia', NULL, NULL),
(9, 'meteorologia/ciencias atmosfericas', NULL, NULL),
(10, 'economia', NULL, NULL),
(11, 'silvicultura', NULL, NULL),
(12, 'bioquimica', NULL, NULL),
(13, 'aprovechamiento forestal', NULL, NULL),
(14, 'tecnologia de la madera', NULL, NULL),
(15, 'legislacion forestal', NULL, NULL),
(16, 'hidrobiologia', NULL, NULL),
(17, 'microbiologia', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `supervisor_especialidad_asignaciones`
--

CREATE TABLE `supervisor_especialidad_asignaciones` (
  `id_supervisor` int(10) UNSIGNED NOT NULL,
  `id_especialidad` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tema_articulos`
--

CREATE TABLE `tema_articulos` (
  `id` int(10) UNSIGNED NOT NULL,
  `tema` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tema_articulos`
--

INSERT INTO `tema_articulos` (`id`, `tema`, `created_at`, `updated_at`) VALUES
(1, 'ecologia forestal', NULL, NULL),
(2, 'manejo forestal', NULL, NULL),
(3, 'servicios ecosistemicos', NULL, NULL),
(4, 'mediciones forestales y planificacion', NULL, NULL),
(5, 'tecnologia forestal', NULL, NULL),
(6, 'economia y politica forestal', NULL, NULL),
(7, 'ciencia y tecnologia de la madera', NULL, NULL),
(8, 'vegetacion', NULL, NULL),
(9, 'sensoramiento remoto', NULL, NULL),
(10, 'vida silvestre', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuarios`
--

CREATE TABLE `tipo_usuarios` (
  `id` int(10) UNSIGNED NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo_usuarios`
--

INSERT INTO `tipo_usuarios` (`id`, `tipo`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Editor', NULL, NULL, NULL),
(2, 'Lector', NULL, NULL, NULL),
(3, 'Critico', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_tipo` int(10) UNSIGNED DEFAULT NULL,
  `id_estado` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `id_tipo`, `id_estado`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, 'manurmu', 'manurmu@gmail.com', NULL, '$2y$10$Tdfmqo4uV7wBuMHBEGC62ecZ42tc1sKoIloQybqpi6k7N3P/ikNIC', 'jxVTdgimtQwetkk7SAusD4T9Lc2irQQsLBRfYVmRF0t4r7BWFcLINCeuBcuC', '2020-10-05 08:03:42', '2020-10-05 08:03:42'),
(2, 3, NULL, 'supervisor', 'supervisor@gmail.com', NULL, '$2y$10$Eh4uPpbjrcIJ/e8G2fx1ouPcK1IN/BZtlTItlVrZxWHPQDi.C8wfu', NULL, '2020-10-05 08:20:15', '2020-10-05 08:20:15'),
(3, 1, NULL, 'jodaurmu', 'jodaurmu@gmail.com', NULL, '$2y$10$CTeH19trkgaDODA.JoxvC.toj2njRTDAacR/gfBRwTFNyCHfWGPb2', NULL, '2020-10-05 08:21:04', '2020-10-05 08:21:04'),
(4, 3, NULL, 'supervisor2', 'supervisor2@gmail.com', NULL, '$2y$10$X./ZfqqddJMBLqAdvNjiKeg5IrB1zqyfFvJ26pLkG2Tl7KrC/l7z.', NULL, '2020-10-05 08:29:18', '2020-10-05 08:29:18'),
(5, 2, NULL, 'user', 'user@gmail.com', NULL, '$2y$10$TqL/tXeKQX2F2dO0MVoTa.3NtXbCxTBK3I3SSJu1UEwY7Jm5mDziC', NULL, '2020-10-24 13:09:27', '2020-10-24 13:09:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_supervisor_asignacion`
--

CREATE TABLE `usuario_supervisor_asignacion` (
  `id` int(10) UNSIGNED NOT NULL,
  `supervisor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creador` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `usuario_supervisor_asignacion`
--

INSERT INTO `usuario_supervisor_asignacion` (`id`, `supervisor`, `creador`, `created_at`, `updated_at`) VALUES
(15, 'supervisor', 'manurmu', '2020-10-24 14:20:54', '2020-10-24 14:20:54');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articulos_titulo_unique` (`titulo`),
  ADD KEY `articulos_id_usuario_foreign` (`id_usuario`),
  ADD KEY `articulos_id_estado_foreign` (`id_estado`);

--
-- Indices de la tabla `asignacion_tema_articulos`
--
ALTER TABLE `asignacion_tema_articulos`
  ADD KEY `asignacion_tema_articulos_titulo_foreign` (`titulo`),
  ADD KEY `asignacion_tema_articulos_id_tema_foreign` (`id_tema`),
  ADD KEY `asignacion_tema_articulos_id_categoria_foreign` (`id_categoria`);

--
-- Indices de la tabla `categoria_articulos`
--
ALTER TABLE `categoria_articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comentarios_articulos`
--
ALTER TABLE `comentarios_articulos`
  ADD KEY `comentarios_articulos_id_articulo_foreign` (`id_articulo`),
  ADD KEY `comentarios_articulos_id_usuario_foreign` (`id_usuario`);

--
-- Indices de la tabla `estado_articulos`
--
ALTER TABLE `estado_articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_usuarios`
--
ALTER TABLE `estado_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `perfil_usuarios`
--
ALTER TABLE `perfil_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `supervisor_especialidades`
--
ALTER TABLE `supervisor_especialidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `supervisor_especialidad_asignaciones`
--
ALTER TABLE `supervisor_especialidad_asignaciones`
  ADD KEY `supervisor_especialidad_asignaciones_id_supervisor_foreign` (`id_supervisor`),
  ADD KEY `supervisor_especialidad_asignaciones_id_especialidad_foreign` (`id_especialidad`);

--
-- Indices de la tabla `tema_articulos`
--
ALTER TABLE `tema_articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_usuarios`
--
ALTER TABLE `tipo_usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_tipo_foreign` (`id_tipo`),
  ADD KEY `users_id_estado_foreign` (`id_estado`);

--
-- Indices de la tabla `usuario_supervisor_asignacion`
--
ALTER TABLE `usuario_supervisor_asignacion`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `categoria_articulos`
--
ALTER TABLE `categoria_articulos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `estado_articulos`
--
ALTER TABLE `estado_articulos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `estado_usuarios`
--
ALTER TABLE `estado_usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `perfil_usuarios`
--
ALTER TABLE `perfil_usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `supervisor_especialidades`
--
ALTER TABLE `supervisor_especialidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `tema_articulos`
--
ALTER TABLE `tema_articulos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tipo_usuarios`
--
ALTER TABLE `tipo_usuarios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuario_supervisor_asignacion`
--
ALTER TABLE `usuario_supervisor_asignacion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD CONSTRAINT `articulos_id_estado_foreign` FOREIGN KEY (`id_estado`) REFERENCES `estado_articulos` (`id`),
  ADD CONSTRAINT `articulos_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `asignacion_tema_articulos`
--
ALTER TABLE `asignacion_tema_articulos`
  ADD CONSTRAINT `asignacion_tema_articulos_id_categoria_foreign` FOREIGN KEY (`id_categoria`) REFERENCES `categoria_articulos` (`id`),
  ADD CONSTRAINT `asignacion_tema_articulos_id_tema_foreign` FOREIGN KEY (`id_tema`) REFERENCES `tema_articulos` (`id`),
  ADD CONSTRAINT `asignacion_tema_articulos_titulo_foreign` FOREIGN KEY (`titulo`) REFERENCES `articulos` (`titulo`);

--
-- Filtros para la tabla `comentarios_articulos`
--
ALTER TABLE `comentarios_articulos`
  ADD CONSTRAINT `comentarios_articulos_id_articulo_foreign` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id`),
  ADD CONSTRAINT `comentarios_articulos_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `perfil_usuarios`
--
ALTER TABLE `perfil_usuarios`
  ADD CONSTRAINT `perfil_usuarios_id_foreign` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `supervisor_especialidad_asignaciones`
--
ALTER TABLE `supervisor_especialidad_asignaciones`
  ADD CONSTRAINT `supervisor_especialidad_asignaciones_id_especialidad_foreign` FOREIGN KEY (`id_especialidad`) REFERENCES `supervisor_especialidades` (`id`),
  ADD CONSTRAINT `supervisor_especialidad_asignaciones_id_supervisor_foreign` FOREIGN KEY (`id_supervisor`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_estado_foreign` FOREIGN KEY (`id_estado`) REFERENCES `estado_usuarios` (`id`),
  ADD CONSTRAINT `users_id_tipo_foreign` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
