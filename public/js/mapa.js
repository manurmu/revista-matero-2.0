$(document).ready(function(){
    var mymap = L.map('mapid').setView([-3.74607, -73.24558], 19);
  
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(mymap);
  
  L.marker([-3.74607, -73.24558]).addTo(mymap)
      .bindPopup('Oficinas UNAP.<br>Peru, Loreto-Iquitos')
      .openPopup();
  });