@extends('templates.header')
@section('titulo','Matero')
@section('contenido')
<div class="container shadow-lg p-3 mb-5 rounded">
    <h1 style="text-align: center">Revista Matero </h1>
    <div class="row align-items-center ">
        <div class="col">
            <img src="{{asset('images/11.jpg')}}" class="shadow rounded mx-auto d-block w-100 h-50" width="400px" alt="">
        </div>
        <div class="col-sm-12 col-md">
            
            <p style="text-align: justify;   "> MATERO  es una revista científica publicada por la Facultad de Ciencias Forestales de la Universidad Nacional de la Amazonía Peruana. Fundada en 1987 con una periodicidad trimestral, y a partir de la reedición en 2020 la periodicidad será cuatrimestral (abril, agosto y diciembre).
                MATERO publicará trabajos originales relacionados con las ciencias forestales, ecológicas, biológicas y ambientales. Las contribuciones podrán ser en las modalidades de artículos, notas técnicas y revisiones en castellano.
                La abreviatura de su título es Matero, que debe ser usado en bibliografías, notas al pie de páginas, leyendas y referencias bibliográficas.</p>
        </div>
        
    </div>
    <br>
    <div class="row align-items-center">
        <div class="col-sm-12 col-md">
            <p style="text-align: justify;   "> MATERO  es una revista científica publicada por la Facultad de Ciencias Forestales de la Universidad Nacional de la Amazonía Peruana. Fundada en 1987 con una periodicidad trimestral, y a partir de la reedición en 2020 la periodicidad será cuatrimestral (abril, agosto y diciembre).
                MATERO publicará trabajos originales relacionados con las ciencias forestales, ecológicas, biológicas y ambientales. Las contribuciones podrán ser en las modalidades de artículos, notas técnicas y revisiones en castellano.
                La abreviatura de su título es Matero, que debe ser usado en bibliografías, notas al pie de páginas, leyendas y referencias bibliográficas.</p>
        
        </div>
        <div class="col-sm-12 col-md">
            <img src="{{asset('images/11.jpg')}}" class="shadow rounded mx-auto d-block w-100 h-50 " width="400px" alt="">
        </div>
    </div>
</div>

    
    <div class="container">
    <div class="row">
        <div class="col">
            <h1>Articulos disponibles en: </h1>
            <div class="card" style="width: 18rem;">
                <img src="{{asset('images/scielo_logo.png')}}" class="card-img-top" alt="...">
                <div class="card-body ">
                  <h5 style="color: black" class="card-title">Scielo</h5>
                  <p style="color: black" class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  <a href="https://scielo.org/en/" class="btn btn-primary">Visitar</a>
                </div>
              </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col">
            <h1>Indizada en: </h1>
            <p>Los articulos publicados en Matero son indizados o resumidos por: </p>
            <div class="card" style="width: 18rem;">
                <img src="{{asset('images/scielo_logo.png')}}" class="card-img-top" alt="...">
                <div class="card-body ">
                  <h5 style="color: black" class="card-title">Scielo</h5>
                  <p style="color: black" class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  <a href="https://scielo.org/en/" class="btn btn-primary">Visitar</a>
                </div>
              </div>
        </div>
    </div>
</div>

@endsection