@extends('templates.header')
@section('titulo','Matero')
@section('contenido')

<div class="container shadow-lg p-3 mb-5 bg-white rounded text-black-50">

@forelse ($Articlechecked as $item)
<div class="row">
    <div class="col ">
        <label for="name">Titulo:</label>
        @php
            $articlename = $item->titulo
        @endphp
<input class="form-control" id="name" type="text" placeholder="{{$item->titulo}}" readonly>
    </div>
</div>
<div class="row">
    <div class="col ">

<div class="form-group">
  <label for="exampleFormControlTextarea1">Abstract: </label>
  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{$item->resumen}}</textarea>
</div>

    </div>
</div>
<div class="row">
  <div class="col">
<label for="content">Temas de Articulo: </label>
@forelse ($articletopic as $items)
<input class="form-control" id="content" type="text" placeholder="{{$items->tema}}" readonly> <br>
    
@empty
    
@endforelse
  
  </div>
</div>
<div class="row">
  <div class="col ">
<label for="content">Tipo de Articulo: </label>

<input class="form-control" id="content" type="text" placeholder=" {{$articlecategory->categoria}}" readonly> <br>
   



  </div>
</div>
<div class="row">
    <div class="col ">
<label for="username"> Usuario:</label>
<input class="form-control" id="username" type="text" placeholder="{{$item->username}}" readonly>
<br>
    </div>
</div>






@if (Auth::user()->id_tipo==2)

<form action="{{route('downloadarticle')}}" method="post">
    @csrf
  <input type="hidden" name="article" value="{{$item->archivo}}">
    <button>Ver Articulo</button>
  </form>
<form action="{{route('updatearticle')}}" method="post" enctype="multipart/form-data">
    @csrf
    
<input type="hidden" name="articletitle" value="{{$item->titulo}}">
<input type="hidden" name="idarticle" value="{{$item->id}}">
<input type="file" class="btn btn-dark" name="article" required id="">
{{$errors}}
    <button class="btn btn-secondary">Actualizar Archivo</button>
</form>
@else
<form action="{{route('downloadarticle')}}" method="post">
    @csrf
  <input type="hidden" name="article" value="{{$item->archivo}}">
    <button>Ver Articulo</button>
  </form>

@endif
<br>
@if (Auth::user()->id_tipo==2 ||Auth::user()->id_tipo==3 )
    
@if ($item->id_estado == 3 or $item->id_estado == 4)
    
@else
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
  Comentar 
</button>
@endif



  <br><br>
 

@if ($numerodeaprobados == 1)
<div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: 33.33%;color:black;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1/3</div>
</div> 
@endif
@if ($numerodeaprobados == 2)
<div class="progress">
  <div class="progress-bar bg-warning" role="progressbar" style="width: 66.66%;color:black;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">2/3</div>
</div>
@endif
@if ($numerodeaprobados == 3)
<div class="progress">
  <div class="progress-bar bg-success" role="progressbar" style="width: 100%;color:black;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">3/3</div>
</div>
@endif
@if ($numerodeaprobados == 0)
<div class="progress">
  <div class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">1/3</div>
</div>
@endif

  

  <br><br>
  
  
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Publicar Articulo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="{{route('createreview')}}" method="post">
                @csrf
                <div class="row">
                    <div class="col justify-content-center">
                        <p>Envie su comentario</p> 
                        <input type="hidden" name="iduser" value="{{Auth::user()->id}}">
                        <input type="hidden" name="idarticle" value="{{$item->id}}">
                        <textarea style="width: 100%" name="content"id="" required ></textarea>
                    </div>
                </div>
          
        </div>

        <div class="modal-footer">
          
          @if ($item->id_estado == '1' and Auth::user()->id_tipo ==2)
          <button class="btn btn-danger" disabled>Enviar comentario</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          @else
          <button class="btn btn-primary">Enviar comentario</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          @endif
                    
                </form>
            
          
          
        </div>
      </div>
    </div>
  </div>

  @endif
@empty
    
@endforelse
@forelse ($getreview as $review)
<div class="row">
    <div class="col">
        <div class="reviews border rounded border-secondary">
            <p class="m-2">{{$review->review}}</p>
            
        <p class="m-2">{{$review->created_at}}</p>

        </div>
        
    </div>
</div>
<br>

@empty
    
@endforelse
</div>
<script src="{{asset('js/navegacion.js')}}"></script> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
@endsection