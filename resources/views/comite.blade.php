@extends('templates.header')
@section('titulo','Matero')
@section('contenido')

<div class="container shadow-lg p-3 mb-5 bg-white rounded">
<div class="row">
    <div class="col">
        <h1  style="text-align: center; color:black">Comite</h1>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="accordion" id="accordionExample">
            <div class="card">
              <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                  <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Director
                  </button>
                </h2>
              </div>
          
              <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Tedi Pacheco Gomez</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                  </div>
                    
                   </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                  <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Subdirector
                  </button>
                </h2>
              </div>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                  <div class="col-12 col-sm-6 col-md-4">
                    <div class="card mx-auto style="width: 18rem;">
                      <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                      <div class="card-body">
                        <h5 class="card-title">Jose David Urquiza Muñoz, MSc</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                      </div>
                    </div>
                  </div>
                  
                  </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                  <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Editores
                  </button>
                </h2>
              </div>
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body text-black-50" >
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Rodil Tellos Espinoza, Dr.</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6  ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Roberto Rojas Ruíz, MSc.</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6  ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Jorge Miguel Espíritu Pezantes, MSc</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Juan Carlos Castro Gómez, Dr.</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Miguel Antonio Puescas Chully, Dr. </h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Hugo Wenceslaos Miguel Miguel, Dr.</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Jomber Chotas Inuma, Dr. </h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Linder Espinoza Marquez, MSc. </h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Rubén Darío Manturano Pérez, Dr. </h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Eric M. Wiener, PhD  </h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 ">
                      <div class="card mx-auto" style="width: 18rem;">
                        <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                        <div class="card-body">
                          <h5 class="card-title">Luís Exequiel Campos Baca</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                      </div> 
                    </div>
                   
                    
                    
                  </div>
                   
                </div>
              </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseOne">
                      Asistente de Comite editor
                    </button>
                  </h2>
                </div>
            
                <div id="collapseFour" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Pedro Anger Angulo Ruiz, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Julio Cesar Bartra Lozano, Lic</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="true" aria-controls="collapseOne">
                      Revisor de Redaccion
                    </button>
                  </h2>
                </div>
            
                <div id="collapseFive" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Anunciacion Hernandez Grandez, DR</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Fernando Guevara Torres</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Ramon Herman Babilonia Sepulveda</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                   </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseOne">
                      Correctores de Ingles
                    </button>
                  </h2>
                </div>
            
                <div id="collapseSix" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Jorge Miguel Espiritu Pezantes, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Ynes Amanda de la Puente Gonzales, Dra</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Julia Victoria Vasquez Villalobos, Dra</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="true" aria-controls="collapseOne">
                      Comite Cientifico Local
                    </button>
                  </h2>
                </div>
            
                <div id="collapseSeven" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Rodil Tello Espinoza</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Roberto Rojas Ruíz, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Tedi Pacheco Gómez, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Juan Carlos Castro Gómez, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Luís Exequiel Campos Baca, Dr </h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Jorge Miguel Espíritu Pezantes, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Richer Ríoss Zumaeta, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Waldemar Alegría Muñoz, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Fredy Francisco Ramírez Arévalo, Ing</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div><div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Lastenia Ruiz Mesía, Dra </h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="true" aria-controls="collapseOne">
                      Comite Cientifico Nacional
                    </button>
                  </h2>
                </div>
            
                <div id="collapseEight" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Rodolfo Vásquez Martinez, Ing</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Casiano Aguierre Escalante, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Hugo Wenceslao Miguel Miguel, Dr </h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Miguel Antonio Puescas Chully, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Emer Ronald Rosales Solórzano, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">German Pérez Hurtado, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Denisse Milagros Alva Mendoza, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                   </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="true" aria-controls="collapseOne">
                      Comite Cientifico Internacional
                    </button>
                  </h2>
                </div>
            
                <div id="collapseNine" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Cesar Alfonso Sabogal, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Rene Lopez Camacho, PhD</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Jomber Chota Inuma, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Augusto Fachin Teran, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Guido Felix Sebastian Kraemer, Ms</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Rafael Villar Montero, PhD</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Miguel Angel Pinedo Vasquez, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Wil de Jong, PhD</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Rene Julio Carmona Cerda, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Miguel Castillo Soto, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Bonifacio Mostacedo Calatayud, PhD</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Roman Ospina Montealegre, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Risto Kalliola, Dr</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Robinson I. Negro-Juarez, PhD</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Daniel Marra, PhD</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-sm-6 col-md-6">
                        <div class="card mx-auto" style="width: 18rem;">
                          <img src="{{asset('images/default.jpeg')}}" class="card-img-top" alt="...">
                          <div class="card-body">
                            <h5 class="card-title">Jose David Urquiza Muñoz, MSc</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" class="btn btn-primary">Go somewhere</a>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                      
                   </div>
                </div>
              </div>
          </div>
    </div>
</div>
</div>
<script src="{{asset('js/navegacion.js')}}"></script> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
@endsection