@extends('templates.header')
@section('titulo','Matero')
@section('contenido')



<div class="container">
  <div class="row">
    <div class="col">
      <h1>Completar Registro</h1>
    </div>
  </div>
<div class="row">
    <div class="col">
        <form action="{{route('completar_registro')}}" method="POST">
          @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                  @error('nombre')<div class="alert-danger">{{ $message }}</div>@enderror
                  <label for="Nombres">Nombres</label>
                  <input type="text" required class="form-control" id="Nombres" name = "nombre" placeholder="Nombres">
                </div>
                <div class="form-group col-md-6">
                  @error('apellido')<div class="alert-danger">{{ $message }}</div>@enderror
                  <label for="Apellidos">Apellidos</label>
                  <input type="text" required class="form-control" id="Apellidos" name = "apellido" placeholder="Apellidos">
                </div>
              </div>
              <div class="form-group">
                <label for="Afiliacion">Afiliacion</label>
                <input type="text" required class="form-control" id="Afiliacion" name = "afiliacion" placeholder="Afiliacion / Trabajador ...">
              </div>
              <div class="form-group">
                <label for="Posicion">Posicion</label>
                <select class="custom-select" name="cargo" required>
                  <option ></option>
                  @forelse ($cargos as $item)
                  <option style="font-size:10px;" value="{{$item->cargo}}">{{$item->cargo}}</option>
                @empty
                @endforelse
                </select>
              </div>
              <div class="form-group">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="publicar_check" name="check_lector" onclick="verpublicar()">
                  <label class="form-check-label" for="publicar_check">
                    Me gustaria publicar aqui. (opcional)
                  </label>
                </div>
              </div>
              <div class="form-group">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="condiciones" required>
                  <label class="form-check-label" for="condiciones">
                    He leido y aceptado los <a target="_blank" href="/politica">Terminos y condiciones de MATERO</a>
                  </label>
                </div>
              </div>
              <script type="text/javascript">
                function verpublicar() {

                    var checkBox = document.getElementById('publicar_check');
                    var campopublicar = document.getElementById('campo_publicar');

                    if (checkBox.checked == true) {
                        campopublicar.style.display = 'block';
                    }
                    else{
                        campopublicar.style.display = 'none';
                    }
                    
                }

              </script>
              <div id="campo_publicar" style="display: none" class="form-group">
                <label for="exampleFormControlTextarea1">¿Por que te gustaria publicar aqui?</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="peticion_cargo" rows="3" placeholder="Asegurate de poner toda la informacion que crees necesaria."></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Aceptar</button>
        </form>
    </div>
</div>    
</div> 



@endsection