@extends('templates.header')
@section('titulo','Matero')
@section('contenido')



<div class="container">
  <div class="row">
    <div class="col">
      <h1>Completar Registro</h1>
    </div>
  </div>
<div class="row">
    <div class="col">
        <form action="{{route('completar_registro_existente_final')}}" method="POST">
          @csrf
          <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Atencion</strong> Si ya solicitaste anteriormente, porfavor espera, que nosotros te responderemos lo mas rapido posible.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
              <div id="campo_publicar"  class="form-group">
                @error('peticion_cargo')<div class="alert-danger">{{ $message }}</div>@enderror
                <label for="exampleFormControlTextarea1">¿Por que te gustaria publicar aqui?</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" name="peticion_cargo" rows="3" required placeholder="Asegurate de poner toda la informacion que crees necesaria."></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Aceptar</button>
        </form>
    </div>
</div>    
</div> 



@endsection