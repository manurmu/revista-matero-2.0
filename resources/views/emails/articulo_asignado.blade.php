@component('mail::message')
# Articulo asignado.

Se te ha asignado un nuevo articulo, revisalo lo mas pronto posible.

@component('mail::button', ['url' => 'https://matero.org/login'])
Ingresa al sistema.
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
