@component('mail::message')
# Nuevo Articulos

El usuario <b>{{ Auth::user()->name }}</b> subio un nuevo articulo.
Ingresa al sistema para revisarlo.

@component('mail::button', ['url' => 'https://matero.org/login'])
Ingresar al sistema.
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
