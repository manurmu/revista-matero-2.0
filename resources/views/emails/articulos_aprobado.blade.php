@component('mail::message')
# Articulo Aprobado

Un articulo ha sido aprobado, por los 3 revisores.


@component('mail::button', ['url' => 'https://matero.org/login'])
Ingresar al sistema.
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
