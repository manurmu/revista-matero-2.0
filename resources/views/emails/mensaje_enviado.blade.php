@component('mail::message')
# Recibiste un nuevo mensaje.

Revisa tus articulos pendientes, tienes una respuesta de uno de tus revisores.

@component('mail::button', ['url' => 'https://matero.org/login'])
Ingresa al sistema
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
