@component('mail::message')
# Peticion Nueva

El usuario <b>{{ Auth::user()->name }}</b> ha realizado una peticion para ser Revisor.
Contacto: <b>{{Auth::user()->email}}</b>

@component('mail::button', ['url' => 'https://matero.org/login'])
Ingrese al sistema.
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
