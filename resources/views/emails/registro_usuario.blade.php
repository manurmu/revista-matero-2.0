@component('mail::message')
# Usuario Registrado

Se ha registrado un nuevo usuario con peticion de publicacion

<div>
    <b> Nombre:</b> @foreach ($nombre as $item){{$item->nombre}}@endforeach
</div>
<div>
    <b> Apellido:</b> @foreach ($apellido as $item){{$item->apellido}}@endforeach
</div>
<div>
    <b> Afiliacion:</b> @foreach ($afiliacion as $item){{$item->afiliacion}}@endforeach
</div>
<div>
    <b> Cargo:</b> @foreach ($cargo as $item){{$item->cargo}}@endforeach
</div>
<div>
    <b> Por que te gustaria publicar aqui?:</b> <br> @foreach ($peticion as $item){{$item->peticion}}@endforeach <br>
</div>
Contacto: <b>{{Auth::user()->email}}</b>


Gracias<br>
{{ config('app.name') }}
@endcomponent
