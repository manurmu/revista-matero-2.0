@extends('templates.header')
@section('titulo','Matero')
@section('contenido')
<div class="container shadow-lg p-3 mb-5 rounded">
    <div class="row">
        <div class="col">
            <h1>Peticion</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
        <h2>{{$username}}</h2>
        <p style="text-transform: capitalize">{{$nombreapellido->nombres}} {{$nombreapellido->apellidos}}</p>
        <p> Quiere ser supervisor en las siguientes especialidades</p>
        <ul class="list-group">
            @forelse ($specialties as $item)
            <li style="color: black" class="list-group-item"> {{$item->especialidad}}</li>
          
              
          @empty
              
          @endforelse
        </ul>
       <br>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
            Aprobar Peticion
          </button>
          
          <!-- Modal -->
          <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 style="color: black" class="modal-title" id="staticBackdropLabel">Peticion de Supervisor</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div style="color: black" class="modal-body">
                  Esta seguro de aprobar al usuario <b>{{$username}}</b> como Supervisor?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  
                <form action="{{route('aprobepetition')}}" method="post">
                      @csrf
                  <input type="hidden" name="username" value="{{$username}}">
                <input type="hidden" name="idusuario" value="{{$idusuario}}">
                  <button  class="btn btn-primary">Aprobar</button>
                </form>
                </div>
              </div>
            </div>
          </div>
          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop2">
            Denegar Peticion
          </button>
          
          <!-- Modal -->
          <div class="modal fade" id="staticBackdrop2" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 style="color: black" class="modal-title" id="staticBackdropLabel">Peticion de Supervisor</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div style="color: black" class="modal-body">
                  Esta seguro de denegar la peticion?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  
                <form action="{{route('denypetition')}}" method="post">
                      @csrf
                  <input type="hidden" name="username" value="{{$username}}">
                  <button  class="btn btn-danger">Denegar</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        
    </div>
   
       
  
</div>   
<script src="{{asset('js/navegacion.js')}}"></script> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
@endsection