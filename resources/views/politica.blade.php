@extends('templates.header')
@section('titulo','Matero')
@section('contenido')
<div class="container">
    <div class="row">
        <div class="col">
            <h2>Politica Editorial</h2>
            <p style="text-align:justify">
                La Revista Matero, publicada por la Facultad de Ciencias Forestales de la Universidad Nacional de la Amazonía Peruana (FCF-UNAP), presenta periodicidad cuatrimestral y se destina a la publicación de artículos científicos, notas técnicas y revisiones que involucran las áreas de las ciencias forestales, ecológicas, biológicas y ambientales. Las fechas de publicación son enero, mayo y setiembre de cada año. Las contribuciones podrán ser en las modalidades de artículos, notas técnicas y revisiones.
Los artículos serán enviados y/o publicados en castellano, y debe ser original, que aún no se hayan notificado o sometidos a la publicación en otra revista o vehículo de divulgación.
Los trabajos aprobados preliminarmente serán enviados a por lo menos dos revisores del área y publicados, solamente si son aprobados por los revisores y por el cuerpo editorial.
La publicación de los artículos se basará en la originalidad, calidad y mérito científico, correspondiendo al comité editorial la decisión final de la aceptación. El secreto de identidad de los autores y revisores se mantendrá durante todo el proceso.
La administración de la Revista Matero, tomará el cuidado para que los revisores de cada artículo sean, obligatoriamente, de instituciones distintas de la de origen de los autores. Artículo que presenta más de cinco autores no tendrá su aplicación aceptada por la Revista Matero, salvo algunas condiciones especiales.
No se permiten cambios en el nombre de autores a posteriori.
Los datos, opiniones y conceptos emitidos en los artículos, así como la exactitud de las referencias bibliográficas, son de entera responsabilidad del (los) autor (es). Sin embargo, el editor, con la asistencia de los Árbitros, el Comité Editorial y el Comité Científico, se reservará el derecho de sugerir o solicitar modificaciones aconsejables o necesarias.
Todos los artículos aprobados y publicados por la Revista Matero desde sus inicios estarán disponibles en el sitio http://matero.unapiquitos.edu.pe/index.php/sistema.
En la presentación en línea, los autores, tiene que considerar los siguientes elementos:
            </p>
            <ul>
                <li>La primera versión del artículo debe omitir los nombres de los autores con sus respectivas notas al pie de página, así como la nota al pie del título;</li>
                <li>Sólo en la versión final, el artículo debe contener el nombre de todos los autores con identificación en nota a pie de página, inclusive la del título;</li>
                <li>Identificación, por medio de asterisco, del autor correspondiente con dirección completa.</li>
            </ul>
            <p style="text-align: justify">
                El Comité Editor revisa el manuscrito para verificar la pertenencia al ámbito de la revista y el cumplimiento de las instrucciones para los autores. Cuando no se cumplen tales condiciones, el manuscrito es devuelto al autor de correspondencia, informándole su situación. Cuando se han cumplido dichas condiciones, el Comité Editor envía el manuscrito a un mínimo de dos árbitros o pares evaluadores externos, en un sistema de doble ciego. Los árbitros evalúan el manuscrito de acuerdo con la pauta que proporciona la revista. La respuesta de los árbitros puede ser: publicar con modificaciones menores, publicar con modificaciones mayores o no publicar. Las observaciones de los árbitros son evaluadas por el Comité Editor, el cual informa por escrito al autor de correspondencia la decisión de continuar o no en el proceso de publicación. Cuando el manuscrito es aceptado, el Comité Editor envía al autor de correspondencia una carta de aceptación de su manuscrito, indicando el tipo de modificación necesaria. En no más de cuatro semanas el autor de correspondencia debe devolver una versión modificada a la revista, para que el Comité Editor analice nuevamente el manuscrito. Los trabajos se publican ordenados de acuerdo con la fecha de aceptación definitiva. Una contribución puede ser rechazada por el Comité Editor en cualquiera de las instancias del proceso de publicación, ya sea por cuestiones de fondo o de forma que no cumplan con las instrucciones para los autores. Los trabajos publicados en MATERO están bajo licencia Creative Commons Perú 2.0
            </p>
            <ul>
                <li>
                    <h5>Articulos</h5>
                    <p style="text-align: justify">Informan acerca de investigaciones inéditas de carácter científico que proyectan el conocimiento actualizado en un campo particular contemplado en los ámbitos de la revista y están sustentados en datos procedimentales propios o generados a partir de otros estudios publicados. La extensión máxima de los manuscritos será de 8.000 palabras, considerando todo su contenido (incluye todos los archivos del manuscrito con sus contenidos completos).</p>
                </li>
                <li>
                    <h5>Notas Tecnicas</h5>
                    <p style="text-align: justify">
                        Describen metodologías o técnicas nuevas en el ámbito de la revista, o bien informan acerca de investigaciones en desarrollo, con resultados preliminares. La extensión máxima de los manuscritos será de 3.000 palabras, considerando todo su contenido.
                    </p>
                </li>
                <li>
                    <h5>Revision</h5>
                    <p style="text-align: justify">
                        Síntesis y discusión de la información científica más actual con respecto a un tema relevante en el ámbito de la revista. La extensión máxima de los manuscritos será de 8.000 palabras, considerando todo su contenido.
                    </p>

                </li>
            </ul>
            <h3>Proceso de Publicacion</h3>
            <p style="text-align: justify">
El cabal cumplimiento de las instrucciones para los autores, se refleja en menores tiempos del proceso editorial. El comité editor revisa el manuscrito para verificar la pertenencia al ámbito de la revista y el cumplimiento de las instrucciones para los autores. Cuando no se cumplen tales condiciones, el manuscrito es devuelto al autor de correspondencia, informándole su situación. Cuando se ha verificado el cumplimiento de dichas condiciones, se registra esa fecha como recepción del manuscrito y el comité editor envía el manuscrito a un mínimo de dos árbitros o revisores externos, en un sistema de doble ciego. A los árbitros se les solicita declinar la revisión de un manuscrito cuando sientan que presentan conflictos de interés o que no podrán realizar una revisión justa y objetiva. Los árbitros evalúan el manuscrito de acuerdo con la pauta que proporciona la revista. Si los árbitros o el comité editor lo estiman pertinente, podrán solicitarla los autores, a través del editor, información adicional sobre el manuscrito (datos, procedimientos, etc.) para su mejor evaluación. La respuesta de los árbitros puede ser: publicar con modificaciones menores, publicar con modificaciones mayores o no publicar. Las observaciones de los árbitros son evaluadas por el comité editor, el cual informa por escrito al autor de correspondencia la decisión de continuar o no en el proceso de publicación y si su manuscrito deberá ser nuevamente evaluado por árbitros. Cuando el manuscrito es aceptado, el comité editor envía al autor de correspondencia una carta de aceptación de su manuscrito, indicando el tipo de modificación necesaria. En no más de ocho semanas el autor de correspondencia debe devolver una versión modificada a la revista, para que el comité editor analice el manuscrito corregido. El comité editor decide el orden en que aparecerán los trabajos publicados en cada número. Una contribución puede ser rechazada por el comité editor en cualquiera de las instancias del proceso de publicación, ya sea por cuestiones de fondo o de forma que no cumplan con las instrucciones para los autores. Ante sospecha de conducta poco ética o deshonesta por parte de los autores que han sometido su manuscrito al proceso de edición, el editor se reserva el derecho de informar a las instituciones patrocinadoras u otras autoridades pertinentes para que realicen la investigación que corresponda.
Los trabajos publicados en Matero están bajo licencia Creative Commons Perú 2.0. Ante cualquier duda se sugiere contactarse con el editor o revisar la información adicional de nuestra página web.
            </p>
            <p style="text-align: justify">
                La versión electrónica de libre acceso de los trabajos completos publicados por Matero, se encuentran en:
            </p>
            <div class="card" style="width: 18rem;">
                <img src="{{asset('images/scielo_logo.png')}}" class="card-img-top" alt="...">
                <div class="card-body ">
                  <h5 style="color: black" class="card-title">Scielo</h5>
                  <p style="color: black" class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  <a href="https://scielo.org/en/" class="btn btn-primary">Visitar</a>
                </div>
              </div>
        </div>
    </div>
</div>

@endsection