@extends('templates.header')
@section('titulo','Matero')
@section('contenido')
<div class="container shadow-lg p-3 mb-5 bg-white rounded">
  <div class="row">
    <div class="col">
      <script type="text/javascript">
       

   
        function peticion(){
          var selectBoxtipo = document.getElementById("selectBoxtipo");
          var selectedValuetipo = selectBoxtipo.options[selectBoxtipo.selectedIndex].value;
          var selectBoxtema = document.getElementById("selectBoxtema");
          var selectedValuetema = selectBoxtema.options[selectBoxtema.selectedIndex].value;
          var texto = document.getElementById("textobusqueda").value;
       
          let url = "{{route('ajaxlistararticulos')}}"
         let data = {
           'tipo' : selectedValuetipo,
           'tema' : selectedValuetema,
           'texto': texto,
           "_token": "{{ csrf_token() }}"
           }
     
         let type = "POST"
         let dataType = "json"
     
          $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
             });
     
         error = function(rpta){mostrarerror(rpta)}
         beforeSend = function(rpta){
          
          $('#articulos').append('cargando')
         }
         success = function(rpta){mostrar(rpta)}
         
         
     
         /* Enviamos los datos al archivo ajax */
     
         $.ajax({data,dataType, url, type, error, success, beforeSend})
     
     
        }
        function mostrarerror(error){
            alert("asdasd")
     
        }
        function mostrar(datos){
          $('#articulos').html('')
          
          let columnas = datos['request']
          
          if (columnas.length > 0) {
            for (let index = 0; index < columnas.length; index++) {
                    
                      
                    
                  
                    let card = '<div id="articulo1'+index+'" class="card publicacion" style=" width: 100%;"> </div>'
                    let body = '<div class="card-body" id="articulo2'+index+'"> </div>'
                    let titulo = '<h2 style="color: black;text-transform:capitalize;" class="card-title"><Strong>'+columnas[index]['titulo']+'</Strong></h2>'
                    let resumen = '<div id="articulo3'+index+'" class="rounded border border-secondary bg-white" style="overflow-y:scroll;height:150px;resize:none;"> </div><h4 style="color: black">Autores:</h4><p style="color: black">'+columnas[index]['autores']+'</p>'
                    let contenidoresumen = '<p style="color: black" class="card-text m-3"><small id="emailHelp" class="form-text text-muted">'+columnas[index]['categoria']+'</small>'+columnas[index]['resumen']+'</p>';
                    let verarticulo = '<br> <form action="{{route('downloadarticle')}}" method="post"> @csrf <input type="hidden" name="article" value="'+columnas[index]['archivo']+'"> <button class="btn btn-success">Ver Articulo</button> </form> <br>'
                   let campousuario = '<div class="row"> <div id="articulo4'+index+'" class="col form-inline">  </div></div>';
                   let imgusuario = '<img src="{{asset('images/12.jpg')}}" width="31px" height="31px" class=" rounded-circle mr-1" alt="">';
                   let ultimodato = '<p  style="font-size: 16px;margin:0;color:black"> <STRong>'+columnas[index]['username']+'</STRong></p> <i class="far fa-clock ml-3"></i> <p style="font-size: 16px;margin:0"><strong>'+columnas[index]['created_at']+'</strong></p><br>  ';
                  let descargas = '<span class="badge badge-success"><i class="fas fa-file-download">Total de descargas:'+columnas[index]['descargas']+'</i> </span>'
                 $('#articulos').append(card)
                 $('#articulo1'+index).append(body)
                 $('#articulo2'+index).append(titulo)
                 $('#articulo2'+index).append(resumen)
                 $('#articulo2'+index).append(verarticulo)
                 $('#articulo3'+index).append(contenidoresumen)
                 $('#articulo2'+index).append(campousuario)
                 $('#articulo4'+index).append(imgusuario)
                 $('#articulo4'+index).append(ultimodato)
                 $('#articulo4'+index).append(descargas)
                }
          }  
          else{
            $('#articulos').append('<h1 style="color: black"> No existen resultados </h1>')
          }   
                 
       }
     </script>
      <ul style="background-color: #226666" class="nav justify-content-center rounded p-3">
        <li class="nav-item ">
          <a class="nav-link text-white disabled" href="#">Buscar Por: </a>
        </li>
        <li class="nav-item ">
          <select id="selectBoxtipo" class="custom-select" name="tipoarticulo" onchange="peticion()" required>
            <option value="0">Tipo de Articulo</option>
            @forelse ($tipoarticulo as $item)
          <option  value="{{$item->categoriaid}}">{{$item->categoria}}
          
          </option>
          
          @empty
          @endforelse
          </select>
          
        </li>
        <li class="nav-item">
          <select id="selectBoxtema" class="custom-select" name="tipoarticulo" onchange="peticion()" required>
            <option value="0" >Tema de Articulo</option>
            @forelse ($temaarticulo as $item)
          <option  value="{{$item->temaid}}">{{$item->tema}}
          
          </option>
          
          @empty
          @endforelse
          </select>
        </li>
        <li class="nav-item">
          
            <div class="input-group">
              <input id="textobusqueda" type="text" class="form-control" placeholder="Palabra clave">
              <div class="input-group-append" id="button-addon4">
                
                <button class="btn btn-secondary" onclick="peticion()" >Buscar</button>
              </div>
            </div>
          
          
        </li>
        
      </ul>
    </div>
  </div>
<br>
    <div class="row">
      
        <div id="articulos" class="col  justify-content-center">
         
          @forelse ($Articlepublished as $article)
              
          
            <div  class="card publicacion" style=" width: 100%;">
               
                <div class="card-body" id="articulo2">
                <h2 style="color: black;text-transform:capitalize;" class="card-title"><Strong>{{$article->titulo}}</Strong></h2>
                <div class="rounded border border-secondary bg-white" style="overflow-y:scroll;height:150px;resize:none;">
                <p style="color: black" class="card-text m-3"><strong><small id="emailHelp" class="form-text text-muted">{{$article->categoria}}</small>  {{$article->resumen}}</p>
                 </div>
               <h4 style="color: black">Autores:</h4>
              <p style="color: black">{{$article->autores}}</p>
              <form action="{{route('downloadarticle')}}" method="post">
                @csrf
              <input type="hidden" name="article" value="{{$article->archivo}}">
                <button class="btn btn-success">Ver Articulo</button>
              </form>
                  <br>
                  <div class="row">
                    <div class="col form-inline">
                    <img src="{{asset('images/12.jpg')}}" width="31px" height="31px" class=" rounded-circle mr-1" alt="">
                    <p style="font-size: 16px;margin:0;color:black"> <STRong>{{$article->username}}</STRong></p>
                    <i style="color: black" class="far fa-clock ml-3"></i>
                    <p style="font-size: 16px;margin:0;margin-right:5px; color:black"><strong>{{$article->created_at}}</strong></p>
                    <span class="badge badge-success"><i class="fas fa-file-download">Total de descargas: {{$article->descargas}}</i> </span>
                    
                    <br>  

                  </div>
                </div>
                  
                </div>
              </div>
              <br>
              @empty
              <h2 style="color: black">No hay articulos publicados</h2>
        
                    
          @endforelse

        </div>
        
    </div>
    <div class="row">
      <div class="col">
        {{$Articlepublished->links()}}
        
        
      </div>
    </div>
</div>

@endsection