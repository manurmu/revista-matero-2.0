@extends('templates.header')
@section('contenido')
<div class="container shadow-lg p-3 mb-5 bg-white rounded">
  @if (Session::has('status'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En hora buena</strong> Mensaje eviado correctamente.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
  <div class="row">
    <div class="col">
        <div class="encabezado-post bg-info border rounded-pill">
            <h2>Bienvenido querido usuario</h2>
            
        </div>
                  
    </div>
</div>
<div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Articulos Nuevos
        </button>
      </h2>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row">
      
          <div class="col form-inline justify-content-center">
           
            @forelse ($articles as $article)
              
          
            <div class="card" style="width: 500px;height:250px">
               
                <div class="card-body">
                <h5 class="card-title">{{$article->titulo}}</h5>
                
                <form method="POST" action="{{route('viewarticle')}}" >
                    @csrf
                    <input type="hidden" value="{{$article->titulo}}" name="articlename">
                <input type="hidden" name="idarticle" value="{{$article->id}}">
                    <button class="btn btn-success">Ver post completo</button>
                </form>
                 
                  
                  @if ($article->id_estado == 1)
                  
                  <h6>Estado <span class="badge badge-secondary">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e;" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i> 
                  
                  @elseif ($article->id_estado == 2)
                  <h6>Estado <span class="badge  badge-warning">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                  @elseif ($article->id_estado == 3)
                  <h6>Estado <span class="badge  badge-primary">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                  @elseif ($article->id_estado == 4)
                  <h6>Estado <span class="badge badge-success">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%);" class="fas fa-circle"></i>
    
                  @endif
                  <br>
                  <div class="row"><div class="col form-inline">
                    <img src="{{asset('images/12.jpg')}}" width="31px" height="31px" class=" rounded-circle mr-1" alt="">
                    <p style="font-size: 12px;margin:0">{{$article->username}}</p>
                    <i class="far fa-clock ml-3"></i>
                    <p style="font-size: 12px;margin:0">{{$article->created_at}}</p><br>  
                  </div></div>
                  
                </div>
              </div>
              @empty
              <h2 style="color: black">No hay articulos Nuevos</h2>
              
          @endforelse
          </div>
      </div>
        
        </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Articulos en Revision
        </button>
      </h2>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <div class="row">
      
          <div class="col form-inline justify-content-center">
            @php
            $contador = 0;
        @endphp
            @forelse ($articlesreviewed as $article)
            @php
            $contador = $contador +1;
        @endphp
          
            <div class="card" style="width: 500px;height:250px">
               
                <div class="card-body">
                <h5 class="card-title">{{$article->titulo}}</h5>
                
                <form method="POST" action="{{route('viewarticle')}}" >
                  @csrf
                  <input type="hidden" value="{{$article->titulo}}" name="articlename">
              <input type="hidden" name="idarticle" value="{{$article->id}}">
                  <button class="btn btn-success">Ver post completo</button>
              </form>
              @if ($article->aprobado == 1)
              <button type="button" class="btn btn-primary" data-toggle="modal" disabled data-target="#exampleModal{{$contador}}">
                Aprobado por ti
              </button>
              @else
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$contador}}">
                Aprobar
              </button>
              @endif
              
              
              <!-- Modal -->
              <div class="modal fade" id="exampleModal{{$contador}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Publicar Articulo</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body text-black-50">
                      Esta seguro de aprobarlo?
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <form method="POST" action="{{route('aprobereview')}}" >
                        @csrf
        
                    <input type="hidden" name="idarticle" value="{{$article->id}}">
                        <button class="btn btn-primary">Aprobar</button>
                    </form>
                      
                    </div>
                  </div>
                </div>
              </div>
             
              
                  
                  @if ($article->id_estado == 1)
                  
                  <h6>Estado <span class="badge badge-secondary">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e;" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i> 
                  
                  @elseif ($article->id_estado == 2)
                  <h6>Estado <span class="badge  badge-warning">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                  @elseif ($article->id_estado == 3)
                  <h6>Estado <span class="badge  badge-primary">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                  @elseif ($article->id_estado == 4)
                  <h6>Estado <span class="badge badge-success">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%);" class="fas fa-circle"></i>
    
                  @endif
                  <br>
                  <div class="row"><div class="col form-inline">
                    <img src="{{asset('images/12.jpg')}}" width="31px" height="31px" class=" rounded-circle mr-1" alt="">
                    <p style="font-size: 12px;margin:0">{{$article->username}}</p>
                    <i class="far fa-clock ml-3"></i>
                    <p style="font-size: 12px;margin:0">{{$article->created_at}}</p><br>  
                  </div></div>
                  
                </div>
              </div>
              @empty
              <h2 style="color: black">No hay articulos en revision</h2>
              
          @endforelse
                
          </div>
      </div>
       
       </div>
    </div>
  </div>
  
</div>
    

    
</div>
<script src="{{asset('js/navegacion.js')}}"></script> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
@endsection