@extends('templates.header')
@section('contenido')
<div class="container shadow-lg p-3 mb-5 bg-white rounded">


    @if (Auth::check())
  @if (Auth::user()->idtype == '5')
 <!-- <button type="button" class="btn btn-primary mx-auto d-block" data-toggle="modal" data-target="#staticBackdrop">
    Solicitar ser supervisor
  </button> -->
  
  <!-- Modal -->
  <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Especialidades</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Maximo 3 Especialidades
        <form action="{{route('assignsupervisor_request')}}" method="post">
            @csrf
          <select class="custom-select" name="specialties1">
            <option ></option>
            @forelse ($specialties as $item)
            <option  value="{{$item->id}}">{{$item->especialidad}}</option>
          @empty
          @endforelse
          </select> <br> <br>
          <select class="custom-select" name="specialties2">
            <option ></option>
            @forelse ($specialties as $item)
            <option  value="{{$item->id}}">{{$item->especialidad}}</option>
          @empty
          @endforelse
          </select> <br> <br>
          <select class="custom-select" name="specialties3">
            <option ></option>
            @forelse ($specialties as $item)
            <option  value="{{$item->id}}">{{$item->especialidad}}</option>
          @empty
          @endforelse
          </select>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="hidden" value="{{Auth::user()->id}}" name="iduser">
        <input type="hidden" value="{{Auth::user()->name}}" name="username">
        <input type="hidden" value="3" name="petition">
            <button class="btn btn-primary">Solicitar</button>
          </form>
         
        </div>
      </div>
    </div>
  </div>
  @else
  <h1>Por el momento no hay nada que hacer</h1>
  
@endif

@endif
</div>
<script src="{{asset('js/navegacion.js')}}"></script> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>

@endsection