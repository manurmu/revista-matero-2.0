@extends('templates.header')
@section('contenido')
<div class="container shadow-lg p-3 mb-5 bg-white rounded">
    <div class="row">
        <div class="col">
            <div class="encabezado-post bg-info border rounded-pill">
                <h2>Bienvenido querido editor</h2>
            </div>
            
        </div>
    </div>
    @if (Session::has('status'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En horabuena</strong> Se ha registrado correctamente.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
@if (Session::has('aprobed'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En horabuena</strong> Se ha registrado un nuevo Supervisor.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
@if (Session::has('denied'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En horabuena</strong> Se ha denegado la peticion.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
    <div class="row">
      <div class="col">
        <p style="text-align: center">
          <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Ver Supervisores</a>
          <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2">Ver Creadores</button>
          <button class="btn btn-primary" type="button" data-toggle="collapse" data-target=".multi-collapse" aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Ver ambos</button>
        
         
          
        </p>
        <div class="row">
          <div class="col justify-content-center">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
              <div class="card card-body">
                <div class="col form-inline justify-content-around">
                  
                  @forelse ($supervisor as $item)
                  <form >
                  
                    @csrf
                  <input type="hidden" name="iduser" id="iduser{{$item->idsupervisor}}" value="{{$item->idsupervisor}}">
                 
                    
                  <button type="button" class="btn btn-primary" data-toggle="modal" onclick='verperfil("{{$item->idsupervisor}}","{{$item->name}}")' data-target="#z">
                    {{$item->name}}
                  </button>
                  
                  <!-- Modal -->
                  
                  
                </form>
               
                    @empty
                        
                    @endforelse
                    <script type="text/javascript">

                        
     
                        function verperfil(id,supervisor){
                          
                          let id_supervisor = id;
                          let nombresupervisor = supervisor;
                          let url = "{{route('verperfilbasico')}}"
                         let data = {
                           'id_usuario' : id_supervisor,
                           'usuario' : nombresupervisor,
                           "_token": "{{ csrf_token() }}"
                           }
                     
                         let type = "POST"
                         let dataType = "json"
                     
                          $.ajaxSetup({
                                 headers: {
                                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                 }
                             });
                     
                         error = function(rpta){mostrarerror(rpta)}
                         beforeSend = function(rpta){console.log(rpta)}
                         success = function(rpta){mostrar(rpta)}
                         
                         
                     
                         /* Enviamos los datos al archivo ajax */
                     
                         $.ajax({data,dataType, url, type, error, success, beforeSend})
                     
                     
                        }
                        function mostrarerror(error){
                            alert(error)
                     
                        }
                        function mostrar(datos){
                          $('#perfilsupervisor').html('')
                          let columnas = datos['request']
                          let numerito = datos['numero']
                          let lista_especialidades = datos['especialidades']
                        
                        
                 for (let index = 0; index < columnas.length; index++) {
                   
                      let descripcion = '<img src="{{asset('images/12.jpg')}}" width="100px" height="100px" class=" rounded-circle mr-1 mx-auto d-block" alt=""> <br>'
                      let Universidad = '<p style="text-align: justify"> <i style="color: black" class="far fa-user"></i> Universidad <br> <i style="color: black" class="far fa-user"></i>Profesor Principal <br> Iquitos, Peru</p> <br><br><br>'
                      let nombre = '<p>Usuario</p>'
                      let usuario = '<p>'+columnas[index]['supervisor']+'</p>'
                      let articulos = '<p>Articulos a cargo:'+numerito[index]['numeroarticulos']+'</p>'
                      let especialista = '<p>Especialista en: </p>'
                      let lista = '<ul id="lista">  </ul>'
                  
                     

                     /* let especialidades = '<ul> <li style="color:black">Matematico</li> <li style="color:black">Fisico</li> </ul>'
                      */
                     $('#perfilsupervisor').append(descripcion)
                     $('#perfilsupervisor').append(Universidad)
                     $('#perfilsupervisor').append(nombre)
                     $('#perfilsupervisor').append(usuario)
                     $('#perfilsupervisor').append(articulos)
                     $('#perfilsupervisor').append(especialista)
                     $('#perfilsupervisor').append(lista)
                     for (let item = 0; item < lista_especialidades.length; item++) {
                      let espec = '<li style="color:black">'+lista_especialidades[item]['especialidad']+'</li>'
                      $('#lista').append(espec)
                     }

                    
                     
                 }
                       }
                     
                     </script>
                    <div class="modal fade" id="z" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Perfil</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" >
                            <div class="container shadow-lg p-3 mb-5 rounded">
                              <div class="row">
                                <div id="perfilsupervisor" class="col">
                                  

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" >Save changes</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                
          
                </div>
            </div>
          </div>
          <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample2">
              <div class="card card-body">
                <div class="col form-inline justify-content-around  ">
                  
                  @forelse ($creator as $item)
                 <form action="{{route('viewprofile')}}" method="post">
                    <!-- <form action="#" method="post">-->
                    @csrf
                    <input type="hidden" name="username" value="{{$item->name}}">
                    
                    <button class="btn btn-primary">
                      {{$item->name}}
                    </button>
                  </form>
                  
                      
                    @empty
                        
                    @endforelse
                  
                  
                </div>

                
                </div>
            </div>
          </div>
        </div>
       

        
      </div>
      
      
    </div>
    <div class="accordion" id="accordionExample">
      <div class="card">
        <div class="card-header" id="headingOne">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Articulos en Revision.
            </button>
          </h2>
        </div>
    
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
          <div class="card-body">
            <div class="row">
              <div class="col form-inline justify-content-center">
                @forelse ($Articlechecked as $article)
              
          
                <div class="card" style="width: 500px;height:250px">
                   
                    <div class="card-body">
                    <h5 class="card-title">{{$article->titulo}}</h5>
                   
                    <form action="{{route('downloadarticle')}}" method="post">
                      @csrf
                    <input type="hidden" name="article" value="{{$article->archivo}}">
                      <button class="btn btn-primary">Ver Articulo</button>
                    </form>
                      @if ($article->id_estado == 1)
                      <h6>Estado <span class="badge badge-secondary">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e;" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i> 
                      @elseif ($article->id_estado == 2)
                      <h6>Estado <span class="badge  badge-warning">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                      @elseif ($article->id_estado == 3)
                      <h6>Estado <span class="badge  badge-primary">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                      @elseif ($article->id_estado == 4)
                      <h6>Estado <span class="badge badge-success">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%);" class="fas fa-circle"></i>
                          
                      
                      @else
                      <i style="color: #8e928e" class="fas fa-circle"></i>
                      <i style="color: #e6ff05" class="fas fa-circle"></i>
                      <i style="color: #003cff" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%)" class="fas fa-circle"></i> 
                      @endif
                      <br>
                      <div class="row"><div class="col form-inline">
                        <img src="{{asset('images/12.jpg')}}" width="31px" height="31px" class=" rounded-circle mr-1" alt="">
                      <p style="font-size: 12px;margin:0">{{$article->username}}</p>
                        <i class="far fa-clock ml-3"></i>
                        <p style="font-size: 12px;margin:0">{{$article->created_at}}</p><br>  
                      </div></div>
                    </div>
                  </div>
                  
                  @empty
                  <h2 style="color: black">No hay articulos para revisar</h2>
                  
              @endforelse
              </div>
            </div>
           
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" id="headingTwo">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              Articulos Aprobados
            </button>
          </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
          <div class="card-body">
            <div class="row">
              <div class="col form-inline justify-content-center">
                @php
            $contador = 0;
        @endphp
                @forelse ($Articleaprobed as $article)
                @php
            $contador = $contador +1;
        @endphp
              
          
                <div class="card" style="width: 500px;height:250px">
                   
                    <div class="card-body">
                    <h5 class="card-title">{{$article->titulo}}</h5>
                    
                    <form action="{{route('downloadarticle')}}" method="post">
                      @csrf
                    <input type="hidden" name="article" value="{{$article->archivo}}">
                      <button class="btn btn-primary">Ver Articulo</button>
                    </form>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal{{$contador}}">
                      Publicar
                    </button>
                    
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal{{$contador}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Publicar Articulo</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body text-black-50">
                            Esta seguro de publicarlo?
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <form action="{{route('changearticlestatus')}}" method="post">
                              @csrf
                              <input type="hidden" name="articleid" value="{{$article->id}}">
                              <button class="btn btn-success">Publicar</button>
                            </form>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                   
                      
                      
                      @if ($article->id_estado == 1)
                      <h6>Estado <span class="badge badge-secondary">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e;" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i> 
                      @elseif ($article->id_estado == 2)
                      <h6>Estado <span class="badge badge-warning">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                      @elseif ($article->id_estado == 3)
                      <h6>Estado <span class="badge badge-primary">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                      @elseif ($article->id_estado == 4)
                      <h6>Estado <span class="badge badge-success">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%);" class="fas fa-circle"></i>
                          
                      
                      @else
                      <i style="color: #8e928e" class="fas fa-circle"></i>
                      <i style="color: #e6ff05" class="fas fa-circle"></i>
                      <i style="color: #003cff" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%)" class="fas fa-circle"></i> 
                      @endif
                      <br>
                      <div class="row"><div class="col form-inline">
                        <img src="{{asset('images/12.jpg')}}" width="31px" height="31px" class=" rounded-circle mr-1" alt="">
                        <p style="font-size: 12px;margin:0">{{$article->username}}</p>
                        <i class="far fa-clock ml-3"></i>
                      <p style="font-size: 12px;margin:0">{{$article->created_at}}</p><br>  
                      </div></div>
                      
                    </div>
                  </div>
                  @empty
                  <h2 style="color: black">No hay articulos aprobados</h2>
                  
              @endforelse
              </div>
            </div>
            
           </div>
        </div>
      </div>
      
      <div class="card">
        <div class="card-header" id="headingThree">
          <h2 class="mb-0">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              Articulos Publicados
            </button>
          </h2>
        </div>
        
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
          <div class="card-body">
            <div class="row">
              <div class="col form-inline justify-content-center">
                @forelse ($Articlepublished as $article)
              
          
                <div class="card" style="width: 500px;height:250px">
                   
                    <div class="card-body">
                    <h5 class="card-title">{{$article->titulo}}</h5>
                    
                    <form action="{{route('downloadarticle')}}" method="post">
                      @csrf
                    <input type="hidden" name="article" value="{{$article->archivo}}">
                      <button class="btn btn-primary">Ver Articulo</button>
                    </form>
                      
                      @if ($article->id_estado == 1)
                      
                      <h6>Estado <span class="badge badge-secondary">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e;" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i> 
                      
                      @elseif ($article->id_estado == 2)
                      <h6>Estado <span class="badge  badge-warning">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                      @elseif ($article->id_estado == 3)
                      <h6>Estado <span class="badge  badge-primary">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                      @elseif ($article->id_estado == 4)
                      <h6>Estado <span class="badge badge-success">{{$article->estado}}</span></h6>
                      <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                      <i style="color: hsl(120, 100%, 50%);" class="fas fa-circle"></i>
    
                      @endif
                      <br>
                      <div class="row"><div class="col form-inline">
                        <img src="{{asset('images/12.jpg')}}" width="31px" height="31px" class=" rounded-circle mr-1" alt="">
                        <p style="font-size: 12px;margin:0">{{$article->username}}</p>
                        <i class="far fa-clock ml-3"></i>
                        <p style="font-size: 12px;margin:0">{{$article->created_at}}</p><br>  
                      </div></div>
                      
                    </div>
                  </div>
                  @empty
                  <h2 style="color: black">No hay articulos publicados</h2>
                 
              @endforelse
              </div>
            </div>
           
           </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <p>
      
          <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Ver solicitudes de supervisor
          </button>
          
        </p>
        <div class="collapse" id="collapseExample">
          <div class="card card-body">
            <ul class="list-group">
              @forelse ($petitions as $item)
              <br>
              <div class="col form-inline">
                <li style="color: black" class="list-group-item">{{$item->name}}</li>
                <form action="{{route('viewpetitions')}}" method="POST">
                  @csrf
                <input type="hidden" value="{{$item->name}}" name="username">
                <input type="hidden"  value="{{$item->id_usuario}}" name="iduser">
                  <button class="btn btn-primary">Ver Solicitud</button>
                  </form>
              </div>
            
              @empty
                  
              @endforelse
             
            </ul>
             </div>
        </div>
       
      </div>
    </div>
    
   
    
      
    
    
    
</div>


@endsection