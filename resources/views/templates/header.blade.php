<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('titulo','MATERO')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/estilos.css')}}">
    <link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Piedra&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
   integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
   crossorigin=""/>
   <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
   integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
   crossorigin=""></script>
    <script src="{{asset('js/jquery-3.5.1.js')}}"></script>
<script src="{{asset('js/headroom.js')}}"></script>
<script src="{{asset('js/dropzone.js')}}"></script>
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
<script src="{{asset('js/mapa.js')}}"></script>
   
</head>
@php
@endphp
<header class="shadow-lg p-3 mb-5  rounded">
  <div  id="app" >
    <nav style="background-color: #003A36" class="navbar navbar-expand-md navbar-light  shadow-sm">
        <div class="container-fluid">
          <div class="col form-inline">
            <a class="navbar-brand" href="{{ url('/') }}">
              
              <img src="{{asset('fcf.jpg')}}" class="rounded-circle mx-auto d-block" height="75px" width="75px;">
  
          </a>
          <h1  class="nombre-colegio text-white">MATERO</h1>
          </div>
            
            <button  class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span   class="navbar-toggler-icon"></span>
            </button>
  
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
  
                </ul>
  
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @if (Route::has('login'))
                  <div class="top-right links form-inline justify-content-around ">
                      @auth
                      <li  class="nav-item nav-link dropdown">
                        @if (Auth::user()->id_tipo == '4' || Auth::user()->id_tipo == '2')
                        <a class="btn btn-success" href="

                        {{ url('/home') }}"> Someter Articulo</a>
                            
                        @endif
                        @if (Auth::user()->id_tipo == '5')
                        <a class="btn btn-success" href="

                        {{ url('/completar-registro-existente') }}"> Someter Articulo</a>
                       
                        @endif
                        
          
                        <a class="nav-link dropdown-toggle far fa-question-circle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          ¿Quienes somos?
                        </a>
                        <div class="dropdown-menu text-white" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{url('/acerca')}}">Nosotros</a>
                          <a class="dropdown-item" href="{{url('/comite')}}">Comite</a>
                          <a class="dropdown-item" href="{{url('/politica')}}">Politica</a>
                         
                        </div>
                      </li>
                          <a class="nav-link text-white" href="{{ url('/home') }}">Home <li class="fas fa-home"></li></a>
                          <a class="nav-link text-white" href="/">Inicio<li class="fas fa-globe"></li></a>
                          <a class="nav-link text-white" href="/published">Publicaciones<li class="far fa-newspaper"></li></a>
  
                      @else
                      
                      

                      <li class="nav-item nav-link dropdown">
                        <a class="btn btn-success" href="
          {{ url('/home') }}"> Someter Articulo</a>
                        <a class="nav-link dropdown-toggle far fa-question-circle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          ¿Quienes somos?
                        </a>
                        <div class="dropdown-menu text-white" aria-labelledby="navbarDropdownMenuLink">
                          <a class="dropdown-item" href="{{url('/acerca')}}">Nosotros</a>
                          <a class="dropdown-item" href="{{url('/comite')}}">Comite</a>
                          <a class="dropdown-item" href="{{url('/politica')}}">Politica</a>
                         
                        </div>
                      </li>
                      
                      <a class="nav-link text-white" href="/"><li class="fas fa-globe"></li>Inicio</a>
                      <a class="nav-link text-white" href="/published"><li class="far fa-newspaper"></li>Publicaciones</a>
          
                          <a class="nav-link text-white" href="{{ route('login') }}">
                            <p style="text-align: end;margin:0"> <span class="">Ingresar</span> </p>
                          </a>
  
                          @if (Route::has('register'))
                              <a class="nav-link text-white"  href="{{ route('register') }}">Register</a>
                          @endif
                      @endauth
                  </div>
              @endif
      <form id="logout-form" action="{{route('logout')}}" method="post" style="display: none">@csrf</form>
      <div class="col">
        @guest
        
        @else
        <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->name }}  <span class="caret"></span>
          </a>
          
          
  
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{url('/perfil')}}">
                 
              Ver Perfil
          </a>
              <a class="dropdown-item" href="{{ route('logout') }}"
                 onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
              </a>
              
              
              
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
          </div>
      </li>
    </ul>
        
        
          </div>
          {{-- <div class="col">
            <button type="button" class="btn btn-success animate__animated animate__heartBeat animate__infinite">
               <span class="badge badge-light">4</span>
            </button>
            
          </div> --}}
          @endguest
          
    </div>
    <br>
  </div>
                    
                </ul>
            </div>


</header>
<br>
<br>
<br>
<br>
<br>
<br><br>

    

  


<body>
  @yield('contenido')
  
</body>

<footer>
<div class="footer-top">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12 segment-one">
        <h3>MATERO</h3>
        
        <p>Calle Pevas cuadra 5</p>
                      <p><strong>Telefono </strong>065 887898</p>
                      <p><strong>Correo Electronico </strong>matero@gmail.com</p> </div>   
      <div class="col-md-3 col-sm-6 col-xs-12 segment-two">
        <h2>MATERO</h3>
          <ul>
            <li><a href="">evento</a></li>
            <li><a href="">evento</a></li>
            <li><a href="">evento</a></li>
            <li><a href="">evento</a></li>
            <li><a href="">evento</a></li>
          </ul>
       </div>   
      <div class="col-md-3 col-sm-6 col-xs-12 segment-three">
        <h2>MATERO</h3>
          <p>Siguenos en nuestras redes sociales</p>
          <a href=""><li class="fab fa-facebook-square"></li></a>
          <a href=""><li class="fab fa-facebook-square"></li></a>
          <a href=""><li class="fab fa-facebook-square"></li></a>
          <a href=""><li class="fab fa-facebook-square"></li></a>
        </div>   
        <div class="col-md-3 col-sm-6 col-xs-12 segment-four">
          <div id="mapid"></div>

          
        
        </div>
     </div>
  </div>
</div>
<p class="footer-bottom-text">Esta revista está bajo una Licencia de Creative Commons Perú 2.0 </p>
<p class="footer-bottom-text">Derechos reservados por &copy;Universidad Nacional de la Amazonia Peruana. 2010-<?php echo date("Y");?></p>









</footer>
<script type="text/javascript">
  
// grab an element
var myElement = document.querySelector("header");
// construct an instance of Headroom, passing the element
var options = {
    // vertical offset in px before element is first unpinned
    offset : 0,
    // or you can specify offset individually for up/down scroll
    offset: {
        up: 100,
        down: 50
    },
    // scroll tolerance in px before state changes
    tolerance : 0,
    // or you can specify tolerance individually for up/down scroll
    tolerance : {
        up : 5,
        down : 0
    },
    // css classes to apply
    classes : {
        // when element is initialised
        initial : "headroom",
        // when scrolling up
        pinned : "headroom--pinned",
        // when scrolling down
        unpinned : "headroom--unpinned",
        // when above offset
        top : "headroom--top",
        // when below offset
        notTop : "headroom--not-top",
        // when at bottom of scroll area
        bottom : "headroom--bottom",
        // when not at bottom of scroll area
        notBottom : "headroom--not-bottom",
        // when frozen method has been called
        frozen: "headroom--frozen",
        // multiple classes are also supported with a space-separated list
        pinned: "headroom--pinned foo bar"
    },
    // element to listen to scroll events on, defaults to `window`
    scroller : window,
    // callback when pinned, `this` is headroom object
    onPin : function() {},
    // callback when unpinned, `this` is headroom object
    onUnpin : function() {},
    // callback when above offset, `this` is headroom object
    onTop : function() {},
    // callback when below offset, `this` is headroom object
    onNotTop : function() {},
    // callback when at bottom of page, `this` is headroom object
    onBottom : function() {},
    // callback when moving away from bottom of page, `this` is headroom object
    onNotBottom : function() {}
};

var headroom  = new Headroom(myElement,options);

// initialise
headroom.init();

</script>
<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap.js')}}"></script> 

</html>
