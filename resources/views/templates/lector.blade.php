@extends('templates.header')
@section('contenido')
<script type="text/javascript">
  $(document).ready(function () {
  bsCustomFileInput.init()
})
</script>
<div class="container shadow-lg p-3 mb-5 bg-white rounded">
    <div class="row">
        <div class="col">
            <div class="encabezado-post bg-info border rounded-pill">
                <h2 class="">Bienvenido querido usuario</h2>
                
            </div>
            <h3 style="text-align: start; color:black" >Estos son tus ultimas publicaciones</h3>           
        </div>
    </div>
    
@if (Session::has('status'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En hora buena</strong> Mensaje enviado correctamente.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
@if (Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En hora buena</strong> Articulo  creado correctamente.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
    
    
    
    <div class="row">
        <div class="col form-inline justify-content-center">
            @forelse ($tableArticle as $article)
            <div class="card" style=" width: 300px">
                <div class="card-body">
                <h5 class="card-title">{{$article->titulo}}</h5>

                <form method="POST" action="{{route('viewarticle')}}" >
                    @csrf
                    <input type="hidden" value="{{$article->titulo}}" name="articlename">
                <input type="hidden" name="idarticle" value="{{$article->article}}">
                    <button class="btn btn-success">Revisar</button>
                </form>
           
                  
                  
                  @if ($article->id_estado == 1)
                  <h6>Estado <span class="badge badge-secondary">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e;" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i> 
                  @elseif ($article->id_estado == 2)
                  <h6>Estado <span class="badge badge-warning">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                  @elseif ($article->id_estado == 3)
                  <h6>Estado <span class="badge  badge-primary">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%); font-size:10px" class="fas fa-circle"></i>
                  @elseif ($article->id_estado == 4)
                  <h6>Estado <span class="badge badge-success">{{$article->estado}}</span></h6>
                  <i style="color: #928e8e; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #e6ff05; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: #003cff; font-size:10px" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%);" class="fas fa-circle"></i>
                      
                  
                  @else
                  <i style="color: #8e928e" class="fas fa-circle"></i>
                  <i style="color: #e6ff05" class="fas fa-circle"></i>
                  <i style="color: #003cff" class="fas fa-circle"></i>
                  <i style="color: hsl(120, 100%, 50%)" class="fas fa-circle"></i> 
                  @endif
                </div>
                <div class="row"><div class="col form-inline">
                    <i class="far fa-clock ml-3"></i>
                    <p style="font-size: 12px;margin:0">{{$article->creado}}</p><br>  
                  </div></div>
              </div>
                
            @empty
                <h1 class="animate__animated animate__bounce" style="color: black">NO TIENES NINGUN ARTICULO</h1>

               
                
            @endforelse
            
        </div>
    </div>
    <div class="row">
        <div class="col form inline">
            <p style="color: black">Empieza a escribir tu articulo o algo</p>
            <div class="form-group">
              
            <form method="POST" action="{{route('createarticle')}}" enctype="multipart/form-data" >
                @csrf
                
                <label for="articletitle" style="color: black">Titulo de tu Articulo</label>
               
                <input id="articletitle" type="text" class=" form-control uppercase" name="articletitle" value="{{ old('articletitle') }}" required >
                @error('articletitle')
                  <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                
                
                <div class="form-group">
                  <label style="color: black" for="content">Abstract: </label>
                  <textarea name="content" value="{{ old('content') }}" class="form-control" rows="3" required></textarea>
                </div>
                <label style="color: black" for="tipo">Tipo de Articulo</label>
                
                <div class="row">
                  <div class="col-11">
                   
                      
                    <script type="text/javascript">

                    $('.uppercase').keyup(function(){
                    $(this).val($(this).val().toUpperCase());
                    var titulo = document.getElementById("articletitle").value;
                    console.log(titulo)
                     });
   
                      function peticion(){
                        var selectBox = document.getElementById("selectBox");
                        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
                     
                        let url = "{{route('ajaxcategoria')}}"
                       let data = {
                         'categoria' : selectBox.options[selectBox.selectedIndex].value,
                         "_token": "{{ csrf_token() }}"
                         }
                   
                       let type = "POST"
                       let dataType = "json"
                   
                        $.ajaxSetup({
                               headers: {
                                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                               }
                           });
                   
                       error = function(rpta){mostrarerror(rpta)}
                       beforeSend = function(rpta){console.log(rpta)}
                       success = function(rpta){mostrar(rpta)}
                       
                       
                   
                       /* Enviamos los datos al archivo ajax */
                   
                       $.ajax({data,dataType, url, type, error, success, beforeSend})
                   
                   
                      }
                      function mostrarerror(error){
                          alert("asdasd")
                   
                      }
                      function mostrar(datos){
                        let columnas = datos['request']
                        $('#cuerpomodal').html('')
                 for (let index = 0; index < columnas.length; index++) {
                    
                     let descripcion = columnas[index]['descripcion']
                     $('#cuerpomodal').append('<p style="color: black">'+descripcion+'</p>')
                     $('#myModal').modal('show')
                   
                     
                 }
                     }
                   
                   </script>
                   <div class="modal fade" id="myModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 style="color: black" class="modal-title" id="exampleModalLabel">Tipo de Articulo</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body" id="cuerpomodal">
                          ...
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                    <select id="selectBox" class="custom-select" name="tipoarticulo" onchange="peticion()" required>
                      <option ></option>
                      @forelse ($tipoarticulo as $item)
                    <option style="text-transform:uppercase;" value="{{$item->categoriaid}}">{{$item->categoria}}
                    
                    </option>
                    
                    @empty
                    @endforelse
                    </select>
                  </div>
                  
                </div>
                
                
                <label style="color: black" for="temas">Temas de Articulo (Maximo 2) </label>
                <select class="custom-select" name="tema1" required>
                    <option ></option>
                    @forelse ($temaarticulo as $item)
                    <option style="text-transform:uppercase;" value="{{$item->temaid}}">{{$item->tema}}</option>
                  @empty
                  @endforelse
                  </select>
                  <br><br>
                  <select class="custom-select" name="tema2">
                    <option ></option>
                    @forelse ($temaarticulo as $item)
                    <option style="text-transform:uppercase;" value="{{$item->temaid}}">{{$item->tema}}</option>
                  @empty
                  @endforelse
                  </select>
                  <br>
                
                <br>

                
                 
                <div class="custom-file">
                 
                  <input type="file" class="custom-file-input" name="article" required id="customFile">
                  <label class="custom-file-label" for="customFile">Subir archivo</label>
                  @error('article')
                  <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                </div>

                <br> <br>
                <label style="color: black" for="">Autores</label> <br>
                <textarea name="author" id="" cols="30" rows="10" placeholder="Los nombres van de esta forma: Jose Maria Quispe FLores, Maria Jose Flores Quispe" required></textarea>
                <br> <br>
                <button class="btn btn-success">Enviar</button>
            </form>
              </div>
             </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-around">
        <div class="col text-center">
            
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
                <i class="fas fa-book"></i>
                Instruccion para Autor
              </button>
              
              <!-- Modal -->
              <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 style="color: black" class="modal-title" id="staticBackdropLabel">Estructura de los manuscritos</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p style="color: black;text-align:justify">La organización de artículos y notas técnicas debe seguir la siguiente estructura:</p>
                     <ul>
                       <li style="color: black;text-align:justify">Titulo <p style="color: black;text-align:justify">
                        El título debe ser preciso y conciso. Elegir con mucho cuidado todas las palabras del título; su asociación con otras palabras debería ser cuidadosamente revisada. Debido al acceso internacional de la revista Matero, se recomienda incluir en el título información relevante sobre la localización geográfica del estudio cuando corresponda.
                        El título debe ser escrito en mayúscula, negrita, centralizada en la página y como máximo con 15 palabras. Con la llamada a pie de página numérica, extraída del título, deben constar informaciones sobre la naturaleza del trabajo (si se extrae de tesis / disertación) y referencias a las instituciones colaboradoras. Los títulos de las demás secciones de la estructura (resumen, palabras clave, abstract, keywords, introducción, material y métodos, resultados, discusión, conclusión, agradecimientos y referencias) deberán ser escritos en letra mayúscula, negrita y justificada a la izquierda.
                         </p></li>

                       <li style="color: black;text-align:justify">Autores <p style="color: black;text-align:justify">
                        Indicar el nombre y apellido de todos los autores con letras minúsculas, con las letras iniciales en mayúscula. Separar los autores con coma. Ordene cada dirección mencionando los datos necesarios, primero la institución matriz (por ejemplo, la universidad) y luego las dependencias dentro de aquella en orden decreciente (por ejemplo, facultad, departamento, laboratorio); a continuación, indique la ciudad y el país de residencia del autor. 
                        Como nota a pie de página en la primera página, indicar, para cada autor, afiliación completa (departamento, centro, institución, ciudad, país), dirección completa y e-mail del autor correspondiente. Este debe ser indicado por un "*". Sólo se aceptarán como máximo cinco autores.
                        En la primera versión del artículo sometido, los nombres de los autores y la nota al pie con las direcciones deben ser omitidos.
                        Para la inserción del nombre (s) del (de los) autor (es) y de la (s) dirección (s) en la versión final del artículo debe observar el patrón en el último número de la Revista Matero (http: // matero. unapiquitos.edu.pe/index.php/sistema).
                      </p></li>
                       <li style="color: black;text-align:justify">Resumen <p style="color: black;text-align:justify">
                        Debe contener el planteamiento del problema, el objetivo, fundamentos metodológicos, resultados y conclusiones más relevantes, con un mínimo de 100 palabras y un máximo de 250 palabras. Evite descripciones largas de métodos y no incluya citas bibliográficas ni los niveles de significancia estadística.
                      </p></li>
                       <li style="color: black;text-align:justify">Palabras Clave <p style="color: black;text-align:justify">
                        Como mínimo tres y como máximo cinco palabras, no incluidos en el título y separados por punto.
                      </p></li>
                       <li style="color: black;text-align:justify">Introduccion <p style="color: black;text-align:justify">
                        Comprende planteamiento del problema, importancia del tema, hipótesis si compete, objetivos, alcances del trabajo y limitaciones para su desarrollo, si es que las hubo. En este capítulo se realizará una síntesis e interpretación de la literatura relacionada directamente con el título y objetivos del trabajo.
                        Debe tener, como máximo, 550 palabras, conteniendo citas actuales que presentes relación con el asunto abordado en la investigación.
                      </p></li>
                       <li style="color: black;text-align:justify">Metodos <p style="color: black;text-align:justify">
                        Proveerá información suficiente y concisa de manera que el problema o experimento pueda ser reproducido o fácilmente entendido por especialistas en la materia. Deberán señalarse claramente las especificaciones técnicas y procedencia de los materiales usados, sin describir materiales triviales. Los organismos bióticos deberán ser convenientemente identificados de acuerdo con las normas internacionales que correspondan. En los métodos empleados se deberá señalar claramente el procedimiento experimental o de captación de datos y los métodos estadísticos, así como los programas computacionales. Si el método no fuese original, se indicará bibliográficamente; si fuera original o modificado se describirá convenientemente. En cualquier caso, la presentación de varios métodos será cronológica.
                      </p></li>
                       <li style="color: black;text-align:justify">Resultados <p style="color: black;text-align:justify">
                        Incluye la presentación sintética, ordenada y elaborada de la información obtenida. Entrega resultados en forma de texto escrito con apoyo de tablas y figuras, si corresponde, conjuntamente con análisis e interpretación de los datos. Se deberá evitar tanto la repetición de detalles dados en otros capítulos como la descripción de aquello que sea evidente al examinar las tablas o figuras que se presenten.
                      </p></li>
                       <li style="color: black;text-align:justify">Discusion <p style="color: black;text-align:justify">
                        Incluye la interpretación integrada de los resultados y, cuando corresponda, la comparación de ellos con los de publicaciones previas. Es un análisis crítico de los resultados de acuerdo con los objetivos y la hipótesis, si fuera el caso. Debe comentarse el significado y la validez de los resultados, de acuerdo con los alcances definidos para el trabajo y los métodos aplicados. En este capítulo no deberán repetirse los resultados obtenidos.
                      </p></li>
                       <li style="color: black;text-align:justify">Conclusiones <p style="color: black;text-align:justify">
                        Se incluirán en forma precisa y concisa aquellas ideas más relevantes que se deriven directamente de lo aportado por el trabajo. Deben dar respuesta a las hipótesis o a los objetivos planteados en la introducción. Deben redactarse en forma clara y objetiva sin incluir citas bibliográficas. Pueden incluir recomendaciones para trabajos futuros.
                      </p></li>
                       <li style="color: black;text-align:justify">Agradecimientos <p style="color: black;text-align:justify">
                        En este acápite se deberán mencionar brevemente a personas e instituciones que contribuyeron con financiamiento u otro tipo de colaboración para la realización del trabajo.
                      </p></li>
                       <li style="color: black;text-align:justify">Referencias <p style="color: black;text-align:justify">
                        Se indicarán las referencias de todas las citas bibliográficas señaladas en el texto, ordenadas alfabéticamente. La precisión y la veracidad de los datos entregados en las referencias bibliográficas son responsabilidad del o los autores de las contribuciones y deben corresponder a publicaciones originales. El número máximo de referencias será de 20 a 30 referencias para artículos y notas técnicas, y de 40 para revisiones. Utilice literatura moderna, relevante y directamente relacionada con su trabajo. Por lo menos 2/3 o 60 por ciento del total de las referencias deberán corresponder a revistas científicas indexadas con fecha de publicación inferior a 10 años. Evite citar resúmenes y trabajos presentados y publicados en congresos y similares.
                        Las referencias, deben ser digitadas en espacio 1,5 cm y separadas entre sí por el mismo espacio (1,5 cm). Necesitan ser presentados en orden alfabético de autores.
                        Se sugiere la cita de una o dos referencias de la revista matero.
                        Para la modalidad de revisión no se exige seguir la estructura indicada anteriormente. En todo caso, deben contener las secciones de título, autores, resumen, palabras clave, introducción, el desarrollo del trabajo adecuadamente dividido en capítulos, agradecimientos y referencias.
                      </p></li>
                     </ul>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Entendido</button>
                      
                    </div>
                  </div>
                </div>
              </div>
              
              
              
        </div>
        <div class="col text-center">
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop1">
            <i class="fas fa-align-left"></i>
            Estilo y Formato
          </button>
          
          <!-- Modal -->
          <div class="modal fade" id="staticBackdrop1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 style="color: black" class="modal-title" id="staticBackdropLabel">Estilo y Formato</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p style="color: black;text-align:justify">
                    En general, el resumen, métodos y resultados del manuscrito deberán estar redactados en tiempo pasado, y la introducción, discusión y conclusiones en tiempo presente. Use tiempo presente cuando se refiera a resultados publicados previamente, esto ayuda a diferenciar entre los hallazgos de su estudio (tiempo pasado) y los hallazgos de otros estudios. En el texto no utilice acrónimos ni abreviaturas, escriba el nombre completo de las cosas; las excepciones que se pueden utilizar son aquellas de dominio global como, por ejemplo, ADN, pH, CO2 y muy pocas otras. Tampoco utilice en el texto los símbolos de los elementos químicos. Acate las reglas gramaticales en todo el manuscrito, incluidos tablas y figuras.
                    La digitación del texto debe estar compuesto en programa Word (DOC o RTF) o compatible y los gráficos en programas compatibles con Windows, como Excel, y formato de imágenes: Figuras (GIF) y Fotos (JPEG). Debe tener un máximo de 20 páginas, A4, digitado en espacio 1,5, fuente Times New Roman, estilo normal, tamaño doce. Todos los márgenes deben tener 2,5 cm. Las páginas y las líneas deben numerarse; los números de páginas deben ser colocadas en la margen inferior, a la derecha y las líneas numeradas de forma continua. Separar los párrafos a renglón seguido y con sangría de ocho caracteres a la izquierda de la primera línea. Debe presentarse en archivos electrónicos con procesador de texto Word o formato RTF.
                    El título principal se escribirá con letras mayúsculas y negritas, centrado. En él deberá omitirse la mención de los autores de nombres científicos, los que, sin embargo, se presentarán la primera vez que se mencionen en el texto a partir de la introducción. En el encabezado superior derecho de cada página debe incluirse un título abreviado con un máximo de 60 caracteres y espacios.
                    Las ecuaciones, se deben escribir utilizando el editor de ecuaciones de Word, con la fuente Times New Roman. Las ecuaciones deben recibir una numeración arábiga cada vez mayor. se numerarán en el margen derecho con corchetes [ ]; en el texto se mencionarán de acuerdo con esta numeración.
                    Las unidades de medidas deberán circunscribirse al Sistema Internacional de unidades (SI). En la notación numérica, los decimales deberán ser separados por coma (,) y las unidades de miles por punto (.). En los textos en inglés, los decimales separados por punto y las unidades de miles por coma. Usar cero al comienzo de números menores a una unidad, incluyendo valores de probabilidad (por ejemplo, P < 0,001).
                    La descripción de los resultados de cada prueba estadística en el texto debe incluir el valor exacto de probabilidad asociado P. Para valores de P menores que 0,001, indique como P < 0,001. En tablas y figuras usar asteriscos para señalar el nivel de significancia de las pruebas estadísticas: * = P < 0,05; ** = P < 0,01; *** = P < 0,001; ns = no significativo.
                    Debe indicarse el nombre científico de todos los organismos biológicos que aparezcan en el texto, de acuerdo con la nomenclatura internacional respectiva. Si un nombre común es usado para una especie, la primera vez que cite en el texto, a partir de la introducción, se debe dar a continuación su nombre científico en cursiva entre paréntesis, por ejemplo, cedro (Cedrela odorata L.). Citas posteriores pueden aparecer con el nombre del género abreviado seguido del adjetivo del nombre científico (por ejemplo, C. odorata), siempre y cuando no produzca confusiones con otras especies citadas en el manuscrito. Al iniciar una oración con el nombre de una especie, escriba su género completo y no lo abrevie con su inicial. En el resumen y en el título no mencione los autores de nombres científicos.
                    En las tablas se deben incluir los datos alfanuméricos ordenados en filas y columnas, escritos con fuente Times New Roman de 12 puntos (mínimo 9 puntos de tamaño), sin negritas. Sólo los encabezamientos de las columnas y los títulos generales se separan con líneas horizontales; las columnas de datos deben separarse por espacios y no por líneas verticales. En las figuras se incluyen otras formas de presentación de datos o información, como gráficos, dibujos, fotografías y mapas. En tablas y figuras se deben incluir los títulos auto explicativos en castellano numerados en forma consecutiva (tabla 1., tabla 2.,... ; figura 1., figura 2.,...). Las figuras llevan el título en el margen inferior y las tablas en el margen superior. Las tablas y figuras deben tener una resolución tal que permitan ser reducidos sin perder legibilidad. Sólo se trabaja en blanco, negro y tonos de grises. El espacio que ocupen tablas y figuras en el trabajo deberá ser menor al 50 % del total del impreso. Incluya en el archivo de texto principal las tablas con sus respectivos títulos, ubicándolos lo más próximo posible después de haberlos citado por primera vez en el texto. Las tablas deben estar en el formato de tablas (editables, no como imágenes). Las figuras deben ser entregadas en un archivo aparte, con un formato editable; su ubicación en el texto principal debe ser informada, incluyendo su título, al igual que las tablas.
                    En las figuras todos los rótulos y leyendas deben estar escritos con letra Times New Roman de tamaño 9 a 12 puntos, sin negrita y respetando la gramática y normas de escritura de la revista Matero. Las figuras pequeñas deberán estar diseñadas con un ancho máximo de 8 cm (una columna en la revista Matero) y las grandes con un máximo de 16 cm de ancho (dos columnas en la revista Matero). Excepcionalmente, una figura podrá tener 23 cm de ancho (y máximo 14 cm de alto) para presentarla en formato apaisado. Organice las figuras reuniendo en una sola aquellos objetos afines (por ejemplo, gráficos de un mismo tipo de información) e identifíquelos con una letra mayúscula (A, B, C...), la que se explicará en el título de la figura.
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Entendido</button>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col text-center">
          
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop2">
            <i class="fas fa-atlas"></i>
            Citas y Referencias
          </button>
          
          <!-- Modal -->
          <div class="modal fade" id="staticBackdrop2" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 style="color: black" class="modal-title" id="staticBackdropLabel">Citas y Referencias</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p style="color: black;text-align:justify">
                    Las citas bibliográficas se indicarán en el texto por el apellido del o los autores, seguido del año de publicación, según el Estilo APA. Del mismo modo, en el capítulo Referencias, las referencias bibliográficas también deben seguir el Estilo APA.
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Entendido</button>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>


@endsection