@extends('templates.header')
@section('titulo','Home')
@section('contenido')
<div class="container">
    <h1>Llena los datos</h1>
    <div class="row">
        <div class="col">
            <div class="card mx-auto" style="width: 18rem;">
                <img src="{{asset('images/profilepicture/default.jpg')}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 style="text-align: center" class="card-title">Usuario</h5>
                  
                  
                </div>
              </div>
        </div>
        <div class="col">
            
        <form action="{{route('llenar')}}" method="POST">
            @csrf
                <div class="row"> 
                  <div class="col">
                    <label for="exampleInputEmail1">Nombres:</label>
                    <input type="text" class="form-control" name="nombre" placeholder="Nombres">
                  </div>
                </div>
                <br>
                <div class="row">
                    <div class="col">
                        <label for="exampleInputEmail1">Apellidos:</label>
                        <input type="text" class="form-control" name="apellido" placeholder="Apellidos">
                      </div>
                </div>
                <br>
                <div class="row">
                    <div class="col">
                        <label for="exampleInputEmail1">Edad:</label>
                        <input type="text" class="form-control" name="edad" placeholder="Edad">
                    </div>
                    <div class="col"></div>
                    <div class="col"></div>
                </div>
                <br>
                <button class="btn btn-success" >Enviar</button>
              </form>
              <br><br>
              @error('nombre')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          @error('apellido')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          @error('edad')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
    </div>
    
</div>






@endsection