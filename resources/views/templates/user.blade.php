@extends('templates.header')
@section('titulo','Home')
@section('contenido')

<div class="container shadow-lg p-3 mb-5 bg-white rounded text-black-50">
    <div class="row">
        <div class="col justify-content-center form-inline">
            <img src="{{asset('images/12.jpg')}}" width="100px" height="100px" class=" rounded-circle mr-1" alt="">
        <h3>{{$username}}</h3>
            <br> <br>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-6 mb-3">
          <label for="validationDefault01">Nombre</label>
          <input class="form-control" id="name" type="text" placeholder="" readonly>
        </div>
        <div class="col-md-6 mb-3">
          <label for="validationDefault02">Apellido</label>
          <input class="form-control" id="name" type="text" placeholder="" readonly>
        </div>
      </div>
      <p>Articulos a cargo:</p>

      @if ($usertype == 3)
      <div class="row">
        <div class="col">

          <h2>Especialidades</h2>
          <ul class="list-group">
            @forelse ($supervisortopics as $item)
            <li class="list-group-item"> {{$item->specialties}}</li>
            
          
          

         
              
          @empty
              
          @endforelse
        </ul>
        </div>
      </div>
      
     
      @else
      <div class="row">
        @forelse ($assigned as $item)
        <div class="col-4">
            <p style="text-align: center" class="m-0">{{$item->titulo}}</p>
        </div>
        <div class="col-4">
            <p style="text-align: center" class="m-0">Supervisado por: </p>
        </div>
        <div class="col-4">
            <p style="text-align: center" class="m-0">{{$item->supervisor}}</p>
        </div>
        <hr class="my-5">
        @empty
                    
        @endforelse
    </div>

    <div class="row align-items-center">
      @php
            $contador = 0;
        @endphp
        @forelse ($unassigned as $item)

        @php
            $contador = $contador +1;
        @endphp
        <div class="col-4 form-inline">
            <p style="text-align: center" class="m-0">{{$item->id}}{{$item->titulo}}</p>
            <form method="POST" action="{{route('viewarticle')}}" >
                    @csrf
                    <input type="hidden" value="{{$item->titulo}}" name="articlename">
            <input type="hidden" name="idarticle" value="{{$item->id}}">
                    <button class="btn btn-success">Revisar</button>
                </form>
        </div>
        <div class="col-4 justify-content-center">
            <p  style="text-align: center" class="m-0">Asignar Supervisor: </p>
        </div>
        <div class="col-4 justify-content-center form-inline">
        <form action="{{route('assignsupervisor')}}" method="post">
            @csrf
        <input type="hidden" name="username" value="{{$username}}">
        <input type="hidden" name="idarticle" value="{{$item->id}}">
            
              
              <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop{{$contador}}">
    Asignar Supervisor
  </button>
  
  <!-- Modal -->
<div class="modal fade" id="staticBackdrop{{$contador}}" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel">Asignar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Esta seguro de esta operacion? <br>
          Seleccione 3 supervisores: <br>
          
          <select class="custom-select" name="supervisor1" required>
            <option ></option>
              @forelse ($supervisors as $item)
              
              <option  value="{{$item->name}}">{{$item->name}}</option>
                  @empty
                      
                  @endforelse
              
              
            </select>
            <select class="custom-select" name="supervisor2" required>
              <option ></option>
                @forelse ($supervisors as $item)
                
                <option  value="{{$item->name}}">{{$item->name}}</option>
                    @empty
                        
                    @endforelse
                
                
              </select>
              <select class="custom-select" name="supervisor3" required>
                <option ></option>
                  @forelse ($supervisors as $item)
                  
                  <option  value="{{$item->name}}">{{$item->name}}</option>
                      @empty
                          
                      @endforelse
                  
                  
                </select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button class="btn btn-primary">Asignar</button>
        </div>
      </div>
    </div>
  </div>
              
            </form>
          
        </div>
        <br><br> <br>
        @empty
        
                   
        @endforelse
    </div>
      @endif

    
    
     
      
     
    




</div>
</div>

<br>


<script src="{{asset('js/navegacion.js')}}"></script> 
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
@endsection

