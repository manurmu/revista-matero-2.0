@extends('templates.header')
@section('titulo','Matero')
@section('contenido')
@if (Session::has('status'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En hora buena</strong> Su solicitud se envio correctamente.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
@if (Session::has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En hora buena</strong> Su peticion se ha enviado correctamente.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
@if (Session::has('registrado'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En hora buena</strong> Se te ha registrado correctamente.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
@if (Session::has('registrado_solicitud'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>En hora buena</strong> Se te ha registrado correctamente y se te respondera al correo usado en tu registro.
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
@endif
<div id="carouselExampleIndicators" class="carousel slide" style ="width: 100%;margin: auto;"data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{asset('images/10.jpg')}}"  style=""  class="d-block mx-auto w-75 mitad" alt="...">
      </div>
      <div class="carousel-item">
        <img src="{{asset('images/11.jpg')}}" style=""  class="d-block mx-auto w-75 mitad" alt="...">
      </div>
      <div class="carousel-item">
        <img src="{{asset('images/12.jpg')}}" style=""  class="d-block  mx-auto w-75 mitad" alt="...">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  
  <br><br>
  
  @if (Auth::check() == false)
  <div class="container">
    <div class="row">
      <div class="col">
    
        
      
      <div style="background-color: cadetblue" class="jumbotron">
                
    
        <h1 class="display-4 animate__animated animate__pulse animate__infinite">Hola, Investigador!</h1>
        <p class="lead">Deseas pertenecer al equipo de investigadores de MATERO?.</p>
        <hr class="my-4">
        <p>Si desesas subir un articulo, por favor llena el siguiente formulario, y te mandaremos una respuesta lo mas rapido posible al correo que usaste en tu registro.</p>
        
        
          <a class="btn btn-success" href="
          
          {{ url('/register') }}"> Someter Articulo</a>
         
        
        
        
      </div>
      
      
    </div>
    </div> </div>
  @endif
  @if (Auth::check())
      @if (Auth::user()->id_tipo == '4')
          
      

      <div class="position-fixed bottom-0 right-0 p-3 animate__animated animate__tada animate__infinite" style="z-index: 5; right: 0; bottom: 0;">
        
      <div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="10000">
        <div class="toast-header" style="background-color: #04be1d">
          
          <strong class="mr-auto text-white">Matero</strong>
          <small class="text-white">Ahora</small>
          <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="toast-body" style="background-color: cadetblue">
          Por favor completa tu <a href="/completar-registro" style="color: black"> registro</a>
        </div>
      </div>
    </div>
    @endif
          
      @endif
  <script type="text/javascript">
  $(document).ready(
    function(){
    $(".toast").toast('show');
}
    );
  
  $(document).ready(
    setInterval(function(){
    $(".toast").toast('show');
},300000)
    );
$(function(){
    $("button").click(function() {
        var fired_button = $(this).val();
        let url = "{{route('cargardescripcion')}}"
         let data = {
           'idtema': fired_button,
           "_token": "{{ csrf_token() }}"
           }
     
         let type = "POST"
         let dataType = "json"
         $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
             });
             error = function(rpta){mostrarerror(rpta)}
         beforeSend = function(rpta){
          
          $('#articulos').append('cargando')
         }
         success = function(rpta){mostrar(rpta)}
         $.ajax({data,dataType, url, type, error, success, beforeSend})
        
    });
});
function mostrarerror(error){
            alert("asdasd")
     
        }
        function mostrar(datos){
          $('#descripcion').html('')
          
          let columnas = datos['request']
          
          
            for (let index = 0; index < columnas.length; index++) {
                    let descripcion = '<p class="text-justify">'+(columnas[index]['descripcion'])+'</p>'
                    let titulo = columnas[index]['tema']
                    let idtema = columnas[index]['id']
                    let tituloH = '<h1>'+titulo+'</h1>'
                $('#descripcion').append(tituloH).hide().fadeIn(300);
                $('#descripcion').append(descripcion).hide().fadeIn(300);
                if (idtema == 1) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/1.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 2) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/2.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 3) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/3.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 4) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/4.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 5) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/5.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 6) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/6.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 7) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/7.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 8) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/8.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 9) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/9.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                    if (idtema == 10) {
                      let imagen = '<img src="{{asset("images/descripcionimagen/10.jpg")}}"  style=""  class="d-block mx-auto w-100 h-50" alt="...">'
                      $('#descripcion').append(imagen);
                    }
                
                }
          
           
                 
       }
    
 
 </script>
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-4 col-md-6 text-center" >
        <div class="btn-group-vertical">
        <button class="btn btn-success" id="1" name="1" value="1">Ecologia Forestal</button> 
        <button class="btn btn-success" id="2" name="2" value="2">Manejo forestal</button>
        <button class="btn btn-success" id="3" name="3" value="3">Servicios Ecosistemicos</button>
        <button class="btn btn-success" id="4" name="4" value="4">Mediciones Forestales y Planificacion</button>
        <button class="btn btn-success" id="5" name="5" value="5">Tecnologia Forestal</button>
        <button class="btn btn-success" id="6" name="6" value="6">Economia y Politica Forestal</button>
        <button class="btn btn-success" id="7" name="7" value="7">Ciencia y Tecnologia de la Madera</button>
        <button class="btn btn-success" id="8" name="8" value="8">Vegetacion</button>
        <button class="btn btn-success" id="9" name="9" value="9">Sensoramiento Remoto</button>
        <button class="btn btn-success" id="10" name="10" value="10">Vida Silvestre</button>
      </div>
      </div>
      <div class="col-lg-8 col-md-6">
        <div style="background-color: rgba(95, 158, 160, 0)" id="descripcion" class="jumbotron">
                
    
          
         
          
           
          
          
          
        </div>
      </div>
    </div>
  </div>
<br>
  <div class="container">
      <div  class="row">
          <div class="col">
              <h1 class="" style="text-align:center;">Noticias o algo</h1>
              
          </div>
      </div>
  </div>
  <br><br>
  <div class="container">
      <div class="row">
          <div class="col-sm-12 col-md">
              <img src="{{asset('images/12.jpg')}}" class="shadow mb-5 mx-auto rounded d-block" width="100%" alt="">
              <p style="text-align:justify;font-size:20px">
                La propuesta de editar la Revista Matero fue de Tedi Pacheco Gómez y de Luciano Cárdenas
Valencia a principios del año 1987, cuando formaban parte del Instituto de Investigación
de Flora y fauna Silvestre de la Facultad de Ingeniería Forestal. Se conversó con el Decano
de la Facultad de Ingeniería Forestal de ese entonces, Ingeniero Roberto Cunibertti Porras,
para bosquejar y editar una revista, quien nos indicó que la idea era excelente. Que ustedes
hagan todo lo que se debe hacer, y que él, buscará el financiamiento para su publicación.
Dicho y hecho, la Revista Matero, se publicó en octubre de 1987: Año 1, Número 1; y en
enero de 1988: Año 1, Número 2, y hasta ahí llegó el sueño dorado que teníamos, frustrado
por el nulo interés de los miembros del Instituto de Investigación Forestal y Fauna Silvestre
y de los sucesivos decanos de la Facultad de Ciencias Forestales. Como verán, en setiembre
                </p>
          </div>
          
          <div class="col-sm-12 col-md">
              <img src="{{asset('images/10.jpg')}}" class="shadow mb-5 mx-auto rounded d-block" width="100%" alt="">
              <p  style="text-align:justify;font-size:20px">
                En la Gestión actual, una de las prioridades que se planteó que la Reedición de la Revista
Matero. Para eso, se consideró conservar algunos rasgos como el nombre y el diseño de
carátula. En ese sentido, la revista será digital y no impreso, publicará artículos científicos,
notas técnicas, y revisiones, se publicará cuatrimestralmente, con un número mínimo de
40 artículos al año. El resto de las condiciones se ajustarán a los criterios de LATINDEX.
En resumen, la Revista Matero, será publicada por la Facultad de Ciencias Forestales (FCF)
de la Universidad Nacional de la Amazonía Peruana (UNAP), presentará periodicidad
cuatrimestral y se destina a proporcionar a la comunidad científica publicaciones de alto
nivel ofreciendo integral y gratuitamente resultados de investigaciones relevantes en el
área de Ciencias Forestales y campos relacionados como las Ciencias Ecológicas,
Ambientales y Biológicas.
                </p>
          </div>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md">
            <img src="{{asset('images/11.jpg')}}" class="shadow mb-5 mx-auto rounded d-block" width="100%" alt="">
           
        </div>
      </div>
      @if (Auth::check())
      @if (Auth::user()->id_tipo == '')
          
      
      <div class="row">
        <div class="col">
          @error('especialidad')
          <div class="alert alert-danger">{{ $message }}</div>
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>La peticion ha fallado</strong> Seleccione los datos correctamente e intente nuevamente.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @enderror
          
         
             
         
          <div style="background-color: cadetblue" class="jumbotron">
            

            <h1 class="display-4">Hola, Investigador!</h1>
            <p class="lead">Deseas pertenecer al equipo de investigadores de MATERO?.</p>
            <hr class="my-4">
            <p>Si desesas ser revisor de los articulos que aqui se crean, por favor llena el siguiente formulario, y te mandaremos una respuesta lo mas rapido posible al correo que usaste en tu registro.</p>
            
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#staticBackdrop">
              Ver mas
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 style="color: black" class="modal-title" id="staticBackdropLabel">Formulario para revisor</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  <form method="POST" action="{{route('peticiondesupervisor')}}" enctype="multipart/form-data">
                    @csrf
                      <div class="form-group">
                        <label style="color: black" for="formGroupExampleInput">Nombres</label>
                        <input type="text" class="form-control" id="formGroupExampleInput" name="nombres" required placeholder="Nombre...">
                      </div>
                      <div class="form-group">
                        <label style="color: black" for="formGroupExampleInput2">Apellidos</label>
                        <input type="text" class="form-control" id="formGroupExampleInput2" name="apellidos" required placeholder="Apellido..">
                      </div>
                      <div class="form-group">
                        <label style="color: black" >Seleccione maximo 3 especialidades:</label>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="1">
                          <label style="color: black" class="form-check-label">
                            Suelos
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="2">
                          <label style="color: black" class="form-check-label">
                            Botanica
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="3">
                          <label style="color: black" class="form-check-label">
                            Inventarios Forestales
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="4">
                          <label style="color: black" class="form-check-label">
                            Analisis Espacial SIG
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="5">
                          <label style="color: black" class="form-check-label">
                            Genetica
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="6">
                          <label style="color: black" class="form-check-label">
                            Estadistica
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="7">
                          <label style="color: black" class="form-check-label">
                            Fauna
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="8">
                          <label style="color: black" class="form-check-label">
                            Fisiologia
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="9">
                          <label style="color: black" class="form-check-label">
                            Meteorologia/Ciencias Atmosfericas
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="10">
                          <label style="color: black" class="form-check-label">
                            Economia
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="11">
                          <label style="color: black" class="form-check-label">
                            Silvicultura
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="12">
                          <label style="color: black" class="form-check-label">
                            Bioquimica
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="13">
                          <label style="color: black" class="form-check-label">
                            Aprovechamiento Forestal
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="14">
                          <label style="color: black" class="form-check-label">
                            Tecnologia de la Madera
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="15">
                          <label style="color: black" class="form-check-label">
                            Legislacion Forestal
                          </label>
                        </div>
                        <div class="form-check">
                          <input type="checkbox" name="especialidad[]" value="16">
                          <label style="color: black" class="form-check-label">
                            Hidrobiologia
                          </label>
                        </div>
                        <div class="form-check">
                          <input  type="checkbox"name="especialidad[]" value="17" >
                          <label style="color: black" class="form-check-label">
                            Microbiologia
                          </label>
                        </div>
                        <script type="text/javascript">
                       $(":checkbox").change(function() {
                        var numberOfChecked = $('input:checkbox:checked').length;
                         if (numberOfChecked  > 3) {
                          $('input:checkbox').attr("disabled", true);
                          alert('Por favor escoge maximo 3 especialidades')
                         }
                         
                       });
                       function limpiar(){
                        $('input:checkbox').attr("disabled", false);
                        $('input:checkbox').prop("checked", false);

                        
                       }
                        function ver_especialidades() {
                          var numberOfChecked = $('input:checkbox:checked').length;
                          var totalCheckboxes = $('input:checkbox').length;
                          var numberNotChecked = totalCheckboxes - numberOfChecked;
                          
                          

                          if (numberOfChecked > 3) {
                            
                          }
                        }
                        </script>
                      </div>
                      <button type="button" class="btn btn-primary" onclick="limpiar()">Limpiar</button>
                    
                  </div>
                  <div class="modal-footer">
                    <button  class="btn btn-secondary"  >Enviar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                  </div>
                
                </form>
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      @endif
  </div>
  
  
    
  

  
@endsection