<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/published', 'PublishArticleController@viewpublishedarticles')->name('published');
Route::post('/home/create', 'HomeController@savearticle')->name('createarticle');
Route::post('/home/article', 'CheckArticlesController@viewarticle')->name('viewarticle');
Route::post('/home/article/aprobe', 'ReviewController@aprobereview')->name('aprobereview');
Route::post('/home/article/review', 'ReviewController@createreview')->name('createreview');
Route::post('/home/article/update', 'HomeController@updatearticle')->name('updatearticle');
Route::post('/home', 'PublishArticleController@changearticlestatus')->name('changearticlestatus');
Route::post('/home/download', 'HomeController@downloadarticle')->name('downloadarticle');
Route::post('/home/profile', 'ProfileController@viewprofile')->name('viewprofile');
Route::post('/home/profile/assign', 'ProfileController@assignsupervisor')->name('assignsupervisor');
Route::post('/home/comite', 'HomeController@viewcomite')->name('viewcomite');
Route::post('/home/supervisor_petitions', 'HomeController@viewpetitions')->name('viewpetitions');
Route::post('/home/supervisor_aprobe', 'HomeController@aprobepetition')->name('aprobepetition');
Route::post('/home/supervisor_deny', 'HomeController@denypetition')->name('denypetition');
Route::post('ajaxRequest', 'HomeController@ajax')->name('ajax');
Route::post('ajaxRequests', 'HomeController@ajaxRequestPost')->name('ajaxx');
Route::post('ajaxtipocategoria', 'HomeController@consultartipocategoria')->name('ajaxcategoria');
Route::post('ajaxlistartodosarticulos', 'PublishArticleController@vertodosarticulos')->name('vertodosarticulos');
Route::post('ajaxlistartodosarticulospor', 'PublishArticleController@ajaxlistararticulos')->name('ajaxlistararticulos');
Route::post('descripcion', 'PublishArticleController@cargardescripcion')->name('cargardescripcion');
Route::post('verperfilbasico', 'ProfileController@verperfilbasico')->name('verperfilbasico');
Route::post('peticiondesupervisor', 'PeticionSupervisor@peticion')->name('peticiondesupervisor');
Route::post('llenardatos', 'LlenarDatos@llenar')->name('llenar');
Route::get('/completar-registro', 'ProfileController@listarcargo')->name('listarcargo');
Route::post('/completarregistro', 'ProfileController@completar_registro')->name('completar_registro');
Route::get('/completar-registro-existente', 'ProfileController@completar_registro_existente')->name('completar_registro_existente');
Route::post('completar-registro-existente', 'ProfileController@completar_registro_existente_final')->name('completar_registro_existente_final');



Route::get('{vista}','VistaController@GetVista');

